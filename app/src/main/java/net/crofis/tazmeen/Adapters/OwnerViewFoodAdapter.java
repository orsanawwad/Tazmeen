package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by orsan on 29-Apr-16.
 */
public class OwnerViewFoodAdapter extends RecyclerView.Adapter<OwnerViewFoodAdapter.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    ArrayList<Food> foods;

    public OwnerViewFoodAdapter(Context context, ArrayList<Food> foods) {
        this.context = context;
        this.foods = foods;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_owner_view_food,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Food current = foods.get(position);

        holder.name.setText(current.getName());
        holder.price.setText(current.getPrice()+"");

        Glide.with(this.context).load(current.getImageUrl()).into(holder.image);

        FoodExtraAdapterForFood adapter = new FoodExtraAdapterForFood(this.context, (ArrayList<FoodExtra>) current.getFoodextras());
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.foodextras.setLayoutManager(llm);
        holder.foodextras.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,price;
        ImageView image;
        RecyclerView foodextras;
        View v;
        public ViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            name = (TextView)itemView.findViewById(R.id.owner_view_food_row_name);
            price = (TextView)itemView.findViewById(R.id.owner_view_food_row_price);
            image = (ImageView)itemView.findViewById(R.id.owner_view_food_row_image);
            foodextras = (RecyclerView)itemView.findViewById(R.id.owner_view_food_row_foodextra_list);
        }
    }
}


/** ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * Second adapter for foodextra CAREFUL WHERE YOU TYPE
 *
 */
class FoodExtraAdapterForFood extends RecyclerView.Adapter<FoodExtraAdapterForFood.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    ArrayList<FoodExtra> foodExtras;

    public FoodExtraAdapterForFood(Context context, ArrayList<FoodExtra> foodExtras) {
        this.context = context;
        this.foodExtras = foodExtras;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_owner_view_food_foodextra,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(foodExtras.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return foodExtras.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.owner_view_food_foodextra_row_name);
        }
    }
}