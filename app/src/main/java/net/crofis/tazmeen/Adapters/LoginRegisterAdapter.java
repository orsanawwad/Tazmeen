package net.crofis.tazmeen.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import net.crofis.tazmeen.Fragments.LoginFragment;
import net.crofis.tazmeen.Fragments.RegisterFragment;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by orsan on 16-Apr-16.
 */
public class LoginRegisterAdapter extends FragmentPagerAdapter {

    public LoginRegisterAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new LoginFragment();
            case 1:
                return new RegisterFragment();
            default:
                return null;
        }
    }
}
