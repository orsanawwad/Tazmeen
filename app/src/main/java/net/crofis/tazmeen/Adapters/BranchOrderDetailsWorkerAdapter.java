package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.Classes.OrderDetails;
import net.crofis.tazmeen.Classes.OrderDetailsFoodExtra;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by orsan on 04-May-16.
 */
/** 1 */
public class BranchOrderDetailsWorkerAdapter extends RecyclerView.Adapter<BranchOrderDetailsWorkerAdapter.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    Order order;

    public BranchOrderDetailsWorkerAdapter(Context context, Order order) {
        this.context = context;
        this.order = order;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_worker_order_details,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderDetails detail = order.getDetails().get(position);
        holder.foodList.setLayoutManager(new LinearLayoutManager(context));

        int count = detail.getFoodExtraDetails().size();

        holder.foodName.setText(count + "x " + detail.getFood().getName());
        BranchOrderWorkerChildAdapter adapter = new BranchOrderWorkerChildAdapter(context,detail);
        holder.foodList.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return order.getDetails().size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView foodName;
        RecyclerView foodList;

        public ViewHolder(View itemView) {
            super(itemView);
            foodName = (TextView)itemView.findViewById(R.id.worker_order_details_row_foodname);
            foodList = (RecyclerView)itemView.findViewById(R.id.worker_order_details_row_parent_sublist);
        }
    }
}

/** 2 */
class BranchOrderWorkerChildAdapter extends RecyclerView.Adapter<BranchOrderWorkerChildAdapter.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    OrderDetails detail;

    public BranchOrderWorkerChildAdapter(Context context, OrderDetails detail) {
        this.context = context;
        this.detail = detail;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_worker_order_details_sublist,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderDetailsFoodExtra foodExtras = detail.getFoodExtraDetails().get(position);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.childAdapter.setLayoutManager(llm);
        BranchOrderWorkerChildFoodExtraAdapter adapter = new BranchOrderWorkerChildFoodExtraAdapter(context, (ArrayList<FoodExtra>) foodExtras.getFoodExtras());
        holder.childAdapter.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return detail.getFoodExtraDetails().size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        RecyclerView childAdapter;
        public ViewHolder(View itemView) {
            super(itemView);
            childAdapter = (RecyclerView)itemView.findViewById(R.id.worker_order_details_row_child_sublist);
        }
    }
}

/** 3 */
class BranchOrderWorkerChildFoodExtraAdapter extends RecyclerView.Adapter<BranchOrderWorkerChildFoodExtraAdapter.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    ArrayList<FoodExtra> extras;

    public BranchOrderWorkerChildFoodExtraAdapter(Context context, ArrayList<FoodExtra> extras) {
        this.context = context;
        this.extras = extras;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_worker_order_details_sublist_foodextras,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FoodExtra current = extras.get(position);
        if(position == 0)holder.foodExtraName.setText("- " + current.getName());
        else holder.foodExtraName.setText(current.getName());
    }

    @Override
    public int getItemCount() {
        return extras.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView foodExtraName;

        public ViewHolder(View itemView) {
            super(itemView);
            foodExtraName = (TextView)itemView.findViewById(R.id.worker_order_details_row_sublist_foodextra);
        }
    }
}