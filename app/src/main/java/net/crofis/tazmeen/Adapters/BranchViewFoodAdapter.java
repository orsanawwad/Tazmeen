package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.FoodPick;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by Windows on 01-May-16.
 */

/** 1 */
public class BranchViewFoodAdapter extends RecyclerView.Adapter<BranchViewFoodAdapter.ViewHolder>{

    private static final String TAG = "BranchViewFoodAdapter";

    Context context;
    LayoutInflater inflater;
    ArrayList<Food> foods;

    ArrayList<BranchViewFoodSubFoodExtraList> adapters;

    ArrayList<Food> choosenFood;
    ArrayList<ArrayList<FoodExtra>> choosenFoodExtras;

    ArrayList<FoodPick> foodPicks;


    RecyclerView parentView;

    public BranchViewFoodAdapter(Context context, ArrayList<Food> foods) {
        this.context = context;
        this.foods = foods;
        inflater = LayoutInflater.from(context);
        choosenFood = new ArrayList<>();
        choosenFoodExtras = new ArrayList<>();
        adapters = new ArrayList<>();
        foodPicks = new ArrayList<>();
    }

    public ArrayList<FoodPick> combineAllPicked(){
        int count = 0;
        foodPicks = null;
        foodPicks = new ArrayList<>();
        for (BranchViewFoodSubFoodExtraList mAdapter :
                adapters) {
            foodPicks.add(new FoodPick(choosenFood.get(count),mAdapter.getAllFoodExtras()));
            count++;
        }
        return foodPicks;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        this.parentView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_branch_view_food,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Food current = foods.get(position);

        final BranchViewFoodSubFoodExtraList adapter = new BranchViewFoodSubFoodExtraList(context, (ArrayList<FoodExtra>) current.getFoodextras());


        holder.foodName.setText(current.getName());
        holder.price.setText("$"+current.getPrice()+"");

        Glide.with(context).load(current.getImageUrl()).asBitmap().into(holder.image);

        holder.foodName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Log.d(TAG, "onCheckedChanged: " + holder.quantity.getText().toString());
                    holder.quantity.setText("1");
                    holder.add.setVisibility(View.VISIBLE);
                    holder.remove.setVisibility(View.VISIBLE);
                    adapter.addOneToAdapter();
                    choosenFood.add(current);
                    adapters.add(adapter);
                    parentView.getLayoutManager().scrollToPosition(position);
                }else {
                    holder.quantity.setText("");
                    adapter.adapters.clear();
                    adapter.notifyDataSetChanged();
                    holder.add.setVisibility(View.GONE);
                    holder.remove.setVisibility(View.GONE);
                    choosenFood.remove(current);
                    adapters.remove(adapter);
                }
            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.quantity.getText().toString().isEmpty()) holder.quantity.setText("1");

                Integer count = Integer.parseInt(holder.quantity.getText().toString());
                count++;
                holder.quantity.setText(count+"");
                adapter.addOneToAdapter();
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.quantity.getText().toString().isEmpty()) holder.quantity.setText("1");
                Integer count = Integer.parseInt(holder.quantity.getText().toString());
                if (count > 1) {
                    count--;
                    holder.quantity.setText(count+"");
                    adapter.removeFromAdapter();
                }
            }
        });

        holder.foodExtraQuantity.setLayoutManager(new LinearLayoutManager(context));
        holder.foodExtraQuantity.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        CheckBox foodName;
        TextView price;
        TextView quantity;
        ImageView add,remove;
        RecyclerView foodExtraQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView)itemView.findViewById(R.id.branch_view_food_row_image);
            foodName = (CheckBox)itemView.findViewById(R.id.branch_view_food_row_name);
            price = (TextView)itemView.findViewById(R.id.branch_view_food_row_price);
            quantity = (TextView)itemView.findViewById(R.id.branch_view_food_row_quantity);
            add = (ImageView)itemView.findViewById(R.id.branch_view_food_row_add);
            remove = (ImageView)itemView.findViewById(R.id.branch_view_food_row_remove);
            foodExtraQuantity = (RecyclerView)itemView.findViewById(R.id.branch_view_food_row_foodlist);
        }
    }
}

/** 2 */
class BranchViewFoodSubFoodExtraList extends RecyclerView.Adapter<BranchViewFoodSubFoodExtraList.ViewHolder>
{
    Context context;
    LayoutInflater inflater;
    ArrayList<BranchViewFoodSubFoodExtraNormal> adapters;
    ArrayList<FoodExtra> extras;

    ArrayList<ArrayList<FoodExtra>> choosenFoodExtras;

    public BranchViewFoodSubFoodExtraList(Context context, ArrayList<FoodExtra> extras) {
        this.context = context;
        this.extras = extras;
        adapters = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    public ArrayList<ArrayList<FoodExtra>> getAllFoodExtras(){
        choosenFoodExtras = null;
        choosenFoodExtras = new ArrayList<>();
        for (BranchViewFoodSubFoodExtraNormal mAdapter :
                adapters) {
            choosenFoodExtras.add(mAdapter.choosenFoodExtra);
        }
        return choosenFoodExtras;
    }

    public void addOneToAdapter(){
        adapters.add(new BranchViewFoodSubFoodExtraNormal(context,extras));
        notifyItemInserted(adapters.size()-1);
    }

    public void removeFromAdapter(){
        adapters.remove(adapters.size()-1);
        notifyItemRemoved(adapters.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_branch_view_food_sublist,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BranchViewFoodSubFoodExtraNormal current = adapters.get(position);
        holder.foodExtraItems.setAdapter(current);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.foodExtraItems.setLayoutManager(manager);
    }

    @Override
    public int getItemCount() {
        return adapters.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        RecyclerView foodExtraItems;

        public ViewHolder(View itemView) {
            super(itemView);
            foodExtraItems = (RecyclerView)itemView.findViewById(R.id.branch_view_food_sublist_row_subfoodextras);
        }
    }
}

/** 3 */
class BranchViewFoodSubFoodExtraNormal extends RecyclerView.Adapter<BranchViewFoodSubFoodExtraNormal.ViewHolder>
{

    Context context;
    LayoutInflater inflater;
    ArrayList<FoodExtra> extras;

    ArrayList<FoodExtra> choosenFoodExtra;

    public BranchViewFoodSubFoodExtraNormal(Context context, ArrayList<FoodExtra> extras) {
        this.context = context;
        this.extras = extras;
        inflater = LayoutInflater.from(context);
        choosenFoodExtra = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_branch_view_food_foodextra,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final FoodExtra current = extras.get(position);
        holder.foodExtraName.setText(current.getName());

        holder.foodExtraName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    choosenFoodExtra.add(current);
                }
                else choosenFoodExtra.remove(current);
            }
        });
    }

    @Override
    public int getItemCount() {
        return extras.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        CheckBox foodExtraName;

        public ViewHolder(View itemView) {
            super(itemView);
            foodExtraName = (CheckBox)itemView.findViewById(R.id.branch_view_food_foodextra_row_name);
        }
    }
}