package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.crofis.tazmeen.Activities.OwnerViewBranchesActivity;
import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by orsan on 29-Apr-16.
 */
public class OwnerViewBranchAdapter extends RecyclerView.Adapter<OwnerViewBranchAdapter.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    ArrayList<Branch> branches;

    public OwnerViewBranchAdapter(ArrayList<Branch> branches, Context context) {
        this.branches = branches;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_owner_view_branch,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Branch branch = branches.get(position);

        holder.address.setText(branch.getAddress());
        if(branch.getIsOpen())
        {
            holder.status.setBackgroundColor(Color.parseColor("#64dd17"));
        } else {
            holder.status.setBackgroundColor(Color.parseColor("#ff0000"));
        }

        Glide.with(context).load(branch.getImageUrl()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return branches.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView address;
        View status;
        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView)itemView.findViewById(R.id.owner_view_branch_row_image);
            address = (TextView)itemView.findViewById(R.id.owner_view_branch_row_address);
            status = itemView.findViewById(R.id.owner_view_branch_row_status);
        }
    }
}
