package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by orsanawwad on 23/05/2016.
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder>{
    
    Context context;
    ArrayList<Branch> branches;
    LayoutInflater inflater;

    public SearchResultAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public SearchResultAdapter(Context context, ArrayList<Branch> branches) {
        this.context = context;
        this.branches = branches;
        inflater = LayoutInflater.from(context);
    }

    public void setBranches(ArrayList<Branch> branches) {

        this.branches = branches;
        notifyDataSetChanged();
//        try {
//            if(branches.size()-1 > getItemCount())
//            {
//                this.branches = branches;
//                notifyItemRangeInserted(getItemCount(),branches.size()-1);
//            }
//            else if (branches.size()-1 < getItemCount())
//            {
//                this.branches = branches;
//                notifyItemRangeRemoved(branches.size()-1,getItemCount());
//            }
//            else notifyDataSetChanged();
//        } catch (Exception e) {
//            notifyDataSetChanged();
//        }
    }

    public ArrayList<Branch> getBranches() {
        return branches;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_search,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Branch current = branches.get(position);
        holder.name.setText(current.getRestaurantName());
        holder.address.setText(current.getAddress());
        Glide.with(context).load(current.getImageUrl()).thumbnail(0.1f).into(holder.image);
        if(current.getIsOpen())
        {
            holder.status.setBackgroundColor(Color.parseColor("#64dd17"));
        } else {
            holder.status.setBackgroundColor(Color.parseColor("#ff0000"));
        }
    }

    @Override
    public int getItemCount() {
        if (branches != null) {
            return branches.size();
        } else return 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        View status;
        ImageView image;
        TextView name;
        TextView address;

        public ViewHolder(View v) {
            super(v);
            status = v.findViewById(R.id.search_row_branch_status);
            image = (ImageView) v.findViewById(R.id.search_row_branch_image);
            name = (TextView) v.findViewById(R.id.search_row_branch_name);
            address = (TextView) v.findViewById(R.id.search_row_branch_address);
        }
    }
}
