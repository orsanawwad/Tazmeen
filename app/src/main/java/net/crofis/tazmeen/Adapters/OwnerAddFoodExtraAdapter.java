package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.backendless.Backendless;

import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by orsan on 26-Apr-16.
 */
public class OwnerAddFoodExtraAdapter extends RecyclerView.Adapter<OwnerAddFoodExtraAdapter.ViewHolder>{

    private static final String TAG = "OwnerAddFoodExtraAdapter";

    Context context;

    LayoutInflater inflater;

    ArrayList<FoodExtra> foodExtras;

    ArrayList<FoodExtra> selected;

    public static Boolean isShown = false;

    public OwnerAddFoodExtraAdapter(ArrayList<FoodExtra> foodExtras, Context context) {
        this.foodExtras = foodExtras;
        this.context = context;
        selected = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_owner_add_food_pickfoodextra,parent,false);
        return new ViewHolder(v);
    }

    public ArrayList<FoodExtra> getSelected() {
        return selected;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.foodExtraBox.setText(foodExtras.get(position).getName());
        holder.foodExtraBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    selected.add(foodExtras.get(position));
                }
                else
                {
                    selected.remove(foodExtras.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return foodExtras.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox foodExtraBox;

        public ViewHolder(View itemView) {
            super(itemView);
            foodExtraBox = (CheckBox) itemView.findViewById(R.id.owner_add_food_row_foodextra);
        }
    }
}
