package net.crofis.tazmeen.Adapters;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.crofis.tazmeen.Activities.BranchManagerActivity;
import net.crofis.tazmeen.Activities.BranchManagerDetailsActivity;
import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.Classes.OrderDetails;
import net.crofis.tazmeen.Classes.OrderDetailsFoodExtra;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by orsan on 04-May-16.
 */
public class BranchOrderWorkerAdapter extends RecyclerView.Adapter<BranchOrderWorkerAdapter.ViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<Order> orders;
    FragmentManager manager;
    Boolean flagIsNotSeller;
    View view;
    AlertDialog dialog;
    Marker marker;
    Boolean warningFlag = false;
    Button callButton;

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    private static final String TAG = "BOWA";

    public BranchOrderWorkerAdapter(Context context, ArrayList<Order> orders, FragmentManager manager, Boolean flagIsNotSeller) {
        this.context = context;
        this.orders = orders;
        this.manager = manager;
        this.flagIsNotSeller = flagIsNotSeller;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_worker_order, parent, false);
        return new ViewHolder(v, manager);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Order current = orders.get(position);
        Backendless.UserService.findById(current.getOwnerId(), new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                String first = response.getProperty("firstName").toString();
                String last = response.getProperty("lastName").toString();

                String name = first + " " + last;
                holder.user = response;
                holder.name.setText(name);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });


        Date date = current.getCreated();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("hh:mm");
        String formatted = format1.format(date.getTime());
        String formatted2 = format2.format(date.getTime());
        holder.date.setText(formatted + "\n" + formatted2);

        int count = 0;
        Double price = 0.0;
        for (OrderDetails detail
                : current.getDetails()) {
            for (OrderDetailsFoodExtra extra :
                    detail.getFoodExtraDetails()) {
                count++;
                price = price + detail.getFood().getPrice();
            }
        }

        holder.totalprice.setText("$" + price);

        holder.totalFoods.setText(count + "x Foods");

        Location loc1 = new Location(LocationManager.GPS_PROVIDER);
        Location loc2 = new Location(LocationManager.GPS_PROVIDER);

        loc1.setLatitude(current.getLocation().getLatitude());
        loc1.setLongitude(current.getLocation().getLongitude());

        try {
            loc2.setLatitude(Starter.Variables.location.getLatitude());
            loc2.setLongitude(Starter.Variables.location.getLongitude());
        } catch (Exception e) {
            if (!warningFlag) {
                Toast.makeText(context, "Please check your location.", Toast.LENGTH_LONG).show();
                ((BranchManagerActivity) context).finish();
                warningFlag = true;
            }
        }
        DecimalFormat df = new DecimalFormat("#.##");
        holder.distance.setText(new DecimalFormat().format(loc1.distanceTo(loc2)) + " M");

        holder.status.setText(current.getStatus());

        switch (current.getStatus()) {
            case "pending":
                holder.status.setBackgroundColor(Color.parseColor("#37474F"));
                break;
            case "accepted":
                holder.status.setBackgroundColor(Color.parseColor("#388E3C"));
                break;
            case "denied":
                holder.status.setBackgroundColor(Color.parseColor("#D32F2F"));
                break;
            case "done":
                holder.status.setBackgroundColor(Color.parseColor("#03A9F4"));
                break;
        }

        /**
         * Colors:
         * pending: #37474F
         * accepted: #388E3C
         * canceled: #D32F2F
         * done: #03A9F4
         *
         */

        holder.address.setText(current.getAddress());

        holder.wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BranchManagerDetailsActivity.class);
                intent.putExtra("objectId", current.getObjectId());
                if (flagIsNotSeller) {
                    intent.putExtra("flagIsNotSeller", true);
                } else {
                    intent.putExtra("flagIsNotSeller", false);
                }
                context.startActivity(intent);
            }
        });

        if (!flagIsNotSeller) {
            holder.wrapper.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.d(TAG, "onLongClick: LONGCLICK");
                    showMap(current, holder);
                    return true;
                }
            });
        }
        if(flagIsNotSeller) {
            String whereClause = "orders.objectId = '"+ current.getObjectId() +"'";
            BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
            Backendless.Persistence.of(Branch.class).find(query, new AsyncCallback<BackendlessCollection<Branch>>() {
                @Override
                public void handleResponse(BackendlessCollection<Branch> response) {
                    if (response.getData().size() == 1)
                    {
                        String whereClause = "branches.objectId = '"+ response.getData().get(0).getObjectId() +"'";
                        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
                        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
                            @Override
                            public void handleResponse(BackendlessCollection<Restaurant> response) {
                                if(response.getData().size() == 1)
                                {
                                    holder.timer.setVisibility(View.VISIBLE);
                                    holder.timer.setText(response.getData().get(0).getName());
                                    holder.timer.setTextSize(22f);
                                }
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.e(TAG, "handleFault: " + fault.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "handleFault: " + fault.getMessage());
                }
            });
        }
    }

    private void showMap(final Order current, final ViewHolder holder) {
        if (view == null)
            try {
                view = inflater.inflate(R.layout.dialog_maponly, null);
                callButton = (Button) view.findViewById(R.id.maponly_dialog_callbutton);
                dialog = new AlertDialog.Builder(context)
                        .setView(view)
                        .create();
            } catch (InflateException e) {
                Log.e(TAG, "showMap: " + e.getMessage());
            }
        MapFragment mapFragment = (MapFragment) manager.findFragmentById(R.id.maponly_dialog_mapview);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.getUiSettings().setMapToolbarEnabled(false);

                if (marker != null) {
                    marker.remove();
                }

                LatLng latLng = new LatLng(current.getLocation().getLatitude(), current.getLocation().getLongitude());
                marker = googleMap.addMarker(new MarkerOptions().position(latLng).title("Order position"));
                CameraUpdate cam_position = CameraUpdateFactory.newLatLng(latLng);
                CameraUpdate cam_zoom = CameraUpdateFactory.zoomTo(16);

                googleMap.animateCamera(cam_zoom);
                googleMap.moveCamera(cam_position);
            }
        });
        dialog.setCancelable(true);
        dialog.show();

        if (holder.user != null) {
            callButton.setText(holder.user.getProperty("phone").toString());
            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_CALL);

                    intent.setData(Uri.parse("tel:" + holder.user.getProperty("phone").toString()));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    context.startActivity(intent);
                }
            });
        } else {
            Toast.makeText(context, "You clicked way too early, dismiss the dialog and wait for the names to show up, then try again...", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView name,date,totalFoods,status,distance,totalprice,address,timer;
        FragmentManager manager;
        CardView wrapper;
        BackendlessUser user;

        public ViewHolder(View itemView,FragmentManager manager) {
            super(itemView);
            this.manager = manager;
            name = (TextView)itemView.findViewById(R.id.worker_order_row_name);
            date = (TextView)itemView.findViewById(R.id.worker_order_row_date);
            totalFoods = (TextView)itemView.findViewById(R.id.worker_order_row_totalfoods);
            status = (TextView)itemView.findViewById(R.id.worker_order_row_status);
            distance = (TextView)itemView.findViewById(R.id.worker_order_row_distance);
            totalprice = (TextView)itemView.findViewById(R.id.worker_order_row_totalprice);
            address = (TextView)itemView.findViewById(R.id.worker_order_row_address);
            wrapper = (CardView) itemView.findViewById(R.id.worker_order_row_layoutwrap);
            timer = (TextView)itemView.findViewById(R.id.worker_order_row_timer);
        }
    }
}
