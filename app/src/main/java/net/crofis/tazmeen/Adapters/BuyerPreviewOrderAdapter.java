package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.Classes.OrderDetails;
import net.crofis.tazmeen.Classes.OrderDetailsFoodExtra;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by Windows on 15-May-16.
 */
public class BuyerPreviewOrderAdapter extends RecyclerView.Adapter<BuyerPreviewOrderAdapter.ViewHolder> {

    Context context;
    LayoutInflater inflater;
    Order order;

    public BuyerPreviewOrderAdapter(Order order, Context context) {
        this.order = order;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_buyer_previeworder,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderDetails details = order.getDetails().get(position);
        String amount = ""+details.getFoodExtraDetails().size();
        String name = amount + "x " + details.getFood().getName();
        holder.name.setText(name);

        holder.list.setLayoutManager(new LinearLayoutManager(context));

        BuyerPreviewOrderExtraParentAdapter adapter = new BuyerPreviewOrderExtraParentAdapter(context, (ArrayList<OrderDetailsFoodExtra>) details.getFoodExtraDetails());
        holder.list.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return order.getDetails().size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        RecyclerView list;
        public ViewHolder(View v) {
            super(v);
            name = (TextView)v.findViewById(R.id.buyer_previeworder_row_foodname);
            list = (RecyclerView)v.findViewById(R.id.buyer_previeworder_row_extralist);
        }
    }
}


class BuyerPreviewOrderExtraParentAdapter extends RecyclerView.Adapter<BuyerPreviewOrderExtraParentAdapter.ViewHolder>
{

    Context context;
    LayoutInflater inflater;
    ArrayList<OrderDetailsFoodExtra> data;

    public BuyerPreviewOrderExtraParentAdapter(Context context, ArrayList<OrderDetailsFoodExtra> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_buyer_previeworder_extra_parent,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderDetailsFoodExtra current = data.get(position);

        LinearLayoutManager llm = new LinearLayoutManager(context);

        llm.setOrientation(LinearLayoutManager.HORIZONTAL);

        holder.parent.setLayoutManager(llm);
        BuyerPreviewOrderExtraChildAdapter adapter = new BuyerPreviewOrderExtraChildAdapter(context, (ArrayList<FoodExtra>) current.getFoodExtras());
        holder.parent.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView parent;
        public ViewHolder(View v) {
            super(v);
            parent = (RecyclerView)v.findViewById(R.id.buyer_previeworder_extra_parent_row_recyclerview_parent);
        }
    }
}


class BuyerPreviewOrderExtraChildAdapter extends RecyclerView.Adapter<BuyerPreviewOrderExtraChildAdapter.ViewHolder>
{

    Context context;
    LayoutInflater inflater;
    Order order;
    ArrayList<FoodExtra> data;

    public BuyerPreviewOrderExtraChildAdapter(Context context, ArrayList<FoodExtra> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_buyer_previeworder_extra_child,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(data.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        public ViewHolder(View v) {
            super(v);
            name = (TextView)v.findViewById(R.id.buyer_previeworder_extra_child_foodextra_name);
        }
    }
}