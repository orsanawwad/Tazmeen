package net.crofis.tazmeen.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.R;

import java.util.ArrayList;

/**
 * Created by orsan on 29-Apr-16.
 */
public class OwnerViewFoodExtraAdapter extends RecyclerView.Adapter<OwnerViewFoodExtraAdapter.ViewHolder>{

    Context context;
    LayoutInflater inflater;
    ArrayList<FoodExtra> extras;

    public OwnerViewFoodExtraAdapter(Context context, ArrayList<FoodExtra> extras) {
        this.context = context;
        this.extras = extras;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_owner_view_foodextra,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(extras.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return extras.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.owner_view_foodextra_row_name);
        }
    }
}
