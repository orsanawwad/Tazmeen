package net.crofis.tazmeen.Fragments;


import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.Messaging;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import net.crofis.tazmeen.Activities.LoginActivity;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.LoadingDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {


    public LoginFragment() {
        // Required empty public constructor
    }

    private static final String TAG = "LoginFragment";

    TextView welcome,forgotpassword;
    TextInputEditText email,password;
    CardView login;
    LoadingDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /** Inflate layout to View v */
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        /** Return the view using initUI with View v */
        return initUI(v);
    }


    private View initUI(View v){
        /** Change welcome text */
        welcome = (TextView)v.findViewById(R.id.login_textview_welcome);
        Typeface font = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/great-vibes.regular.ttf");
        welcome.setTypeface(font);

        /** Find EditText views */
        email = (TextInputEditText)v.findViewById(R.id.fragment_login_email);
        password = (TextInputEditText)v.findViewById(R.id.fragment_login_password);
        login = (CardView)v.findViewById(R.id.fragment_login_submit);
        forgotpassword = (TextView)v.findViewById(R.id.login_textview_forgotpassword);

        /** Set click listener to register */
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });

        /** Set on click listener on forgot password to popup edit text to submit email */
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder change = new AlertDialog.Builder(getContext());
                change.setTitle("Forgot password");
                change.setMessage("Enter your email");
                final EditText emailEditText = new EditText(getContext());


                change.setView(emailEditText);

                change.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String resetEmail = emailEditText.getText().toString();

                        final LoadingDialog d = new LoadingDialog(getContext(),"Sending email..");

                        d.show();
                        d.getMessage().setText("Submitting request.");
                        Backendless.UserService.restorePassword(resetEmail, new AsyncCallback<Void>() {
                            @Override
                            public void handleResponse(Void response) {
                                Toast.makeText(getContext(), "We sent you an email, if you're registered at tazmeen, you should get an email shortly", Toast.LENGTH_LONG).show();
                                d.dismiss();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Toast.makeText(getContext(), "We sent you an email, if you're registered at tazmeen, you should get an email shortly", Toast.LENGTH_LONG).show();
                                d.dismiss();
                            }
                        });

                    }
                });

                change.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                        dialog.cancel();
                    }
                });

                change.show();
            }
        });


        /** Add Watchers to the EditTexts */

        /** Watcher to check for email input (basicly checks for @ sign (thanks google default LoginActivity in android sdk!)) */
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().contains("@"))
                {
                    email.setError("Please enter a valid Email");
                }
            }
        });

        /** Check if Password field is empty */
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty() )
                {
                    password.setError("Please enter a password");
                }
            }
        });
        return v;
    }


    /**
     * Log in the user and set CurrentUser in backendless if details correct
     */
    private void logIn() {

        dialog = new LoadingDialog(getContext(),"Logging in...");

        dialog.show();

        String tEmail = email.getText().toString().trim();
        String tPassword = password.getText().toString();

        dialog.getMessage().setText("Checking data...");
        if(!tEmail.contains("@"))
        {
            email.setError("Please enter a valid email");
            dialog.dismiss();
        }
        else {
            try {
                dialog.getMessage().setText("Contacting the server...");
                Backendless.UserService.login(tEmail, tPassword, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser backendlessUser) {
                        dialog.getMessage().setText("Success, updating info...");
                        Backendless.UserService.setCurrentUser(backendlessUser);
                        Starter.Variables.user_role = Integer.parseInt(backendlessUser.getProperty("userRole").toString());
                        backendlessUser.setProperty("currentDevice", Messaging.DEVICE_ID);
                        Backendless.UserService.update(backendlessUser, new AsyncCallback<BackendlessUser>() {
                            @Override
                            public void handleResponse(BackendlessUser response) {
                                dialog.getMessage().setText("Done.");
                                dialog.dismiss();
                                ((LoginActivity)getActivity()).endActivity();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.e(TAG, "handleFault: " + fault.getMessage());
                                Toast.makeText(getContext(), fault.getMessage(), Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        //TODO: add check code if it was email or password error, display snackbar if snackbar is not supporter,
                        //TODO: use toast, else display toast only, highlight also the editTest?
                        email.setError("Recheck your email");
                        password.setError("Recheck your password");
                        dialog.dismiss();
                        Toast.makeText(getContext(), backendlessFault.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "handleFault: " + backendlessFault.getMessage() );
                    }
                }, true);
            } catch (Exception e) {
                Log.d("USER", "ERROR: " + e.getMessage());
                dialog.dismiss();
            }
        }
    }
}