package net.crofis.tazmeen.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.LoadingDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {


    public RegisterFragment() {
        // Required empty public constructor
    }

    View v;
    TextView registrationTitle;
    TextInputEditText firstName,lastName,userName,password,email,phone;
    CardView register;
    LoadingDialog dialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /** Inflate the layout to View v*/
        v = inflater.inflate(R.layout.fragment_register, container, false);

        return initUI(v);
    }

    private View initUI(View v)
    {

        /** Change registration font to something fancy */
        registrationTitle = (TextView)v.findViewById(R.id.fragment_register_title);
        Typeface font = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/great-vibes.regular.ttf");
        registrationTitle.setTypeface(font);

        /** Find the elements */

        /** Find all the EditTexts*/
        firstName = (TextInputEditText)v.findViewById(R.id.fragment_register_firstname);
        lastName = (TextInputEditText)v.findViewById(R.id.fragment_register_lastname);
        userName = (TextInputEditText)v.findViewById(R.id.fragment_register_username);
        password = (TextInputEditText)v.findViewById(R.id.fragment_register_password);
        email = (TextInputEditText)v.findViewById(R.id.fragment_register_email);
        phone = (TextInputEditText)v.findViewById(R.id.fragment_register_phone);

        /** Find the Register Button */
        register = (CardView) v.findViewById(R.id.fragment_register_button);

        /** Set Watchers to EditTexts */

        /** Check username length */
        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty() || s.toString().trim().length() < Starter.Constants.REGISTER_USERNAME_LENGTH_MIN || s.toString().trim().length() > Starter.Constants.REGISTER_USERNAME_LENGTH_MAX)
                {
                    userName.setError("Username length must be between 6 to 20");
                }
            }
        });

        /** Check email if its correct */
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().contains("@"))
                {
                    email.setError("Please enter a valid Email");
                }
            }
        });


        /** Check password length */
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty() || s.toString().length() < Starter.Constants.REGISTER_PASSWORD_LENGTH_MIN || s.toString().length() > Starter.Constants.REGISTER_PASSWORD_LENGTH_MAX)
                {
                    password.setError("Password length must be between 8 to 32");
                }
            }
        });

        /** Check if phone is empty */

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty())
                {
                    phone.setError("You must enter a valid phone number");
                }
            }
        });

        /** Set ClickListener to register button */
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });


        return v;
    }

    private void registerUser() {
        /** Check the inputs */

        String tFirstName = firstName.getText().toString();
        String tLastName = lastName.getText().toString();
        String tUsername = userName.getText().toString();
        String tPassword = password.getText().toString();
        String tEmail = email.getText().toString().trim();
        String tPhone = phone.getText().toString().trim();

        if(tUsername.length() < 6 || tUsername.length() > 20 )
        {
            userName.setError("Username length must be between 8 to 20");
        }
        else if(tPassword.length() < 8 || tPassword.length() > 32)
        {
            password.setError("Password length must be between 8 to 32");
        }
        else if(!tEmail.contains("@"))
        {
            email.setError("Please enter a valid Email");
        }
        else if(tPhone.isEmpty())
        {
            phone.setError("Please enter a valid phone number");
        }
        else {

            dialog = new LoadingDialog(getContext(),"Registering");

            dialog.show();

            BackendlessUser user = new BackendlessUser();
            user.setEmail(tEmail);
            user.setPassword(tPassword);
            user.setProperty("userName",tUsername);
            user.setProperty("firstName",tFirstName);
            user.setProperty("lastName",tLastName);
            user.setProperty("userRole",Starter.Constants.USER_ROLE_NORMAL);
//            user.setProperty("currentDevice", Messaging.DEVICE_ID);
            user.setProperty("phone",tPhone);

            dialog.getMessage().setText("Registering...");
            Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser backendlessUser) {
//                    Snackbar snackbar = Snackbar.make(((LoginActivity)getActivity()).get)
                    dialog.complete(true);
                    Toast.makeText(getActivity().getApplicationContext(), "Successfully registered " + backendlessUser.getEmail() + ", Please confirm your email address then login", Toast.LENGTH_SHORT).show();
                }

                //TODO: Use snackbar instead of toast
                @Override
                public void handleFault(BackendlessFault backendlessFault) {
                    Toast.makeText(getActivity().getApplicationContext(), backendlessFault.getMessage(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        }
    }

}
