package net.crofis.tazmeen.Classes;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class Branch
{
  private java.util.Date updated;
  private String objectId;
  private Boolean isOpen;
  private java.util.Date created;
  private String address;
  private String imageUrl;
  private String restaurantName;
  private String ownerId;
  private java.util.List<Order> orders;
  private GeoPoint location;
  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public Boolean getIsOpen()
  {
    return isOpen;
  }

  public void setIsOpen( Boolean isOpen )
  {
    this.isOpen = isOpen;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getAddress()
  {
    return address;
  }

  public void setAddress( String address )
  {
    this.address = address;
  }

  public String getRestaurantName() {
    return restaurantName;
  }

  public void setRestaurantName(String restaurantName) {
    this.restaurantName = restaurantName;
  }

  public String getImageUrl()
  {
    return imageUrl;
  }

  public void setImageUrl( String imageUrl )
  {
    this.imageUrl = imageUrl;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.List<Order> getOrders()
  {
    return orders;
  }

  public void setOrders( java.util.List<Order> orders )
  {
    this.orders = orders;
  }

  public GeoPoint getLocation()
  {
    return location;
  }

  public void setLocation( GeoPoint location )
  {
    this.location = location;
  }

                                                    
  public Branch save()
  {
    return Backendless.Data.of( Branch.class ).save( this );
  }

  public Future<Branch> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Branch> future = new Future<Branch>();
      Backendless.Data.of( Branch.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Branch> callback )
  {
    Backendless.Data.of( Branch.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Branch.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Branch.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Branch.class ).remove( this, callback );
  }

  public static Branch findById( String id )
  {
    return Backendless.Data.of( Branch.class ).findById( id );
  }

  public static Future<Branch> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Branch> future = new Future<Branch>();
      Backendless.Data.of( Branch.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Branch> callback )
  {
    Backendless.Data.of( Branch.class ).findById( id, callback );
  }

  public static Branch findFirst()
  {
    return Backendless.Data.of( Branch.class ).findFirst();
  }

  public static Future<Branch> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Branch> future = new Future<Branch>();
      Backendless.Data.of( Branch.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Branch> callback )
  {
    Backendless.Data.of( Branch.class ).findFirst( callback );
  }

  public static Branch findLast()
  {
    return Backendless.Data.of( Branch.class ).findLast();
  }

  public static Future<Branch> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Branch> future = new Future<Branch>();
      Backendless.Data.of( Branch.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Branch> callback )
  {
    Backendless.Data.of( Branch.class ).findLast( callback );
  }

  public static BackendlessCollection<Branch> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Branch.class ).find( query );
  }

  public static Future<BackendlessCollection<Branch>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Branch>> future = new Future<BackendlessCollection<Branch>>();
      Backendless.Data.of( Branch.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Branch>> callback )
  {
    Backendless.Data.of( Branch.class ).find( query, callback );
  }
}