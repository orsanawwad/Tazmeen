package net.crofis.tazmeen.Classes;

import java.util.ArrayList;

/**
 * Created by orsan on 02-May-16.
 */
public class FoodPick {
    private Food food;
    private ArrayList<ArrayList<FoodExtra>> allExtras;

    public FoodPick(){

    }

    public FoodPick(Food food, ArrayList<ArrayList<FoodExtra>> allExtras) {
        this.food = food;
        this.allExtras = allExtras;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public ArrayList<ArrayList<FoodExtra>> getAllExtras() {
        return allExtras;
    }

    public void setAllExtras(ArrayList<ArrayList<FoodExtra>> allExtras) {
        this.allExtras = allExtras;
    }
}
