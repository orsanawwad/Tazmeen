package net.crofis.tazmeen.Classes;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class Food
{
  private java.util.Date created;
  private String name;
  private String objectId;
  private java.util.Date updated;
  private String imageUrl;
  private Double price;
  private String ownerId;
  private java.util.List<FoodExtra> foodextras;
  public java.util.Date getCreated()
  {
    return created;
  }

  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getImageUrl()
  {
    return imageUrl;
  }

  public void setImageUrl( String imageUrl )
  {
    this.imageUrl = imageUrl;
  }

  public Double getPrice()
  {
    return price;
  }

  public void setPrice( Double price )
  {
    this.price = price;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public java.util.List<FoodExtra> getFoodextras()
  {
    return foodextras;
  }

  public void setFoodextras( java.util.List<FoodExtra> foodextras )
  {
    this.foodextras = foodextras;
  }

                                                    
  public Food save()
  {
    return Backendless.Data.of( Food.class ).save( this );
  }

  public Future<Food> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Food> future = new Future<Food>();
      Backendless.Data.of( Food.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<Food> callback )
  {
    Backendless.Data.of( Food.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( Food.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( Food.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( Food.class ).remove( this, callback );
  }

  public static Food findById( String id )
  {
    return Backendless.Data.of( Food.class ).findById( id );
  }

  public static Future<Food> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Food> future = new Future<Food>();
      Backendless.Data.of( Food.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<Food> callback )
  {
    Backendless.Data.of( Food.class ).findById( id, callback );
  }

  public static Food findFirst()
  {
    return Backendless.Data.of( Food.class ).findFirst();
  }

  public static Future<Food> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Food> future = new Future<Food>();
      Backendless.Data.of( Food.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<Food> callback )
  {
    Backendless.Data.of( Food.class ).findFirst( callback );
  }

  public static Food findLast()
  {
    return Backendless.Data.of( Food.class ).findLast();
  }

  public static Future<Food> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Food> future = new Future<Food>();
      Backendless.Data.of( Food.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<Food> callback )
  {
    Backendless.Data.of( Food.class ).findLast( callback );
  }

  public static BackendlessCollection<Food> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( Food.class ).find( query );
  }

  public static Future<BackendlessCollection<Food>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<Food>> future = new Future<BackendlessCollection<Food>>();
      Backendless.Data.of( Food.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Food>> callback )
  {
    Backendless.Data.of( Food.class ).find( query, callback );
  }
}