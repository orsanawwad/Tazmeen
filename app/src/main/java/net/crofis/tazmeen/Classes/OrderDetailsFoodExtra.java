package net.crofis.tazmeen.Classes;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class OrderDetailsFoodExtra
{
  private java.util.Date updated;
  private String ownerId;
  private String objectId;
  private java.util.Date created;
  private java.util.List<FoodExtra> foodExtras;
  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.List<FoodExtra> getFoodExtras()
  {
    return foodExtras;
  }

  public void setFoodExtras( java.util.List<FoodExtra> foodExtras )
  {
    this.foodExtras = foodExtras;
  }

                                                    
  public OrderDetailsFoodExtra save()
  {
    return Backendless.Data.of( OrderDetailsFoodExtra.class ).save( this );
  }

  public Future<OrderDetailsFoodExtra> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetailsFoodExtra> future = new Future<OrderDetailsFoodExtra>();
      Backendless.Data.of( OrderDetailsFoodExtra.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<OrderDetailsFoodExtra> callback )
  {
    Backendless.Data.of( OrderDetailsFoodExtra.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( OrderDetailsFoodExtra.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( OrderDetailsFoodExtra.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( OrderDetailsFoodExtra.class ).remove( this, callback );
  }

  public static OrderDetailsFoodExtra findById( String id )
  {
    return Backendless.Data.of( OrderDetailsFoodExtra.class ).findById( id );
  }

  public static Future<OrderDetailsFoodExtra> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetailsFoodExtra> future = new Future<OrderDetailsFoodExtra>();
      Backendless.Data.of( OrderDetailsFoodExtra.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<OrderDetailsFoodExtra> callback )
  {
    Backendless.Data.of( OrderDetailsFoodExtra.class ).findById( id, callback );
  }

  public static OrderDetailsFoodExtra findFirst()
  {
    return Backendless.Data.of( OrderDetailsFoodExtra.class ).findFirst();
  }

  public static Future<OrderDetailsFoodExtra> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetailsFoodExtra> future = new Future<OrderDetailsFoodExtra>();
      Backendless.Data.of( OrderDetailsFoodExtra.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<OrderDetailsFoodExtra> callback )
  {
    Backendless.Data.of( OrderDetailsFoodExtra.class ).findFirst( callback );
  }

  public static OrderDetailsFoodExtra findLast()
  {
    return Backendless.Data.of( OrderDetailsFoodExtra.class ).findLast();
  }

  public static Future<OrderDetailsFoodExtra> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetailsFoodExtra> future = new Future<OrderDetailsFoodExtra>();
      Backendless.Data.of( OrderDetailsFoodExtra.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<OrderDetailsFoodExtra> callback )
  {
    Backendless.Data.of( OrderDetailsFoodExtra.class ).findLast( callback );
  }

  public static BackendlessCollection<OrderDetailsFoodExtra> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( OrderDetailsFoodExtra.class ).find( query );
  }

  public static Future<BackendlessCollection<OrderDetailsFoodExtra>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<OrderDetailsFoodExtra>> future = new Future<BackendlessCollection<OrderDetailsFoodExtra>>();
      Backendless.Data.of( OrderDetailsFoodExtra.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<OrderDetailsFoodExtra>> callback )
  {
    Backendless.Data.of( OrderDetailsFoodExtra.class ).find( query, callback );
  }
}