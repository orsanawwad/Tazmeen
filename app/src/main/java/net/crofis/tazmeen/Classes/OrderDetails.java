package net.crofis.tazmeen.Classes;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class OrderDetails
{
  private java.util.Date updated;
  private String ownerId;
  private String objectId;
  private java.util.Date created;
  private java.util.List<OrderDetailsFoodExtra> foodExtraDetails;
  private Food food;
  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.List<OrderDetailsFoodExtra> getFoodExtraDetails()
  {
    return foodExtraDetails;
  }

  public void setFoodExtraDetails( java.util.List<OrderDetailsFoodExtra> foodExtraDetails )
  {
    this.foodExtraDetails = foodExtraDetails;
  }

  public Food getFood()
  {
    return food;
  }

  public void setFood( Food food )
  {
    this.food = food;
  }

                                                    
  public OrderDetails save()
  {
    return Backendless.Data.of( OrderDetails.class ).save( this );
  }

  public Future<OrderDetails> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetails> future = new Future<OrderDetails>();
      Backendless.Data.of( OrderDetails.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<OrderDetails> callback )
  {
    Backendless.Data.of( OrderDetails.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( OrderDetails.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( OrderDetails.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( OrderDetails.class ).remove( this, callback );
  }

  public static OrderDetails findById( String id )
  {
    return Backendless.Data.of( OrderDetails.class ).findById( id );
  }

  public static Future<OrderDetails> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetails> future = new Future<OrderDetails>();
      Backendless.Data.of( OrderDetails.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<OrderDetails> callback )
  {
    Backendless.Data.of( OrderDetails.class ).findById( id, callback );
  }

  public static OrderDetails findFirst()
  {
    return Backendless.Data.of( OrderDetails.class ).findFirst();
  }

  public static Future<OrderDetails> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetails> future = new Future<OrderDetails>();
      Backendless.Data.of( OrderDetails.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<OrderDetails> callback )
  {
    Backendless.Data.of( OrderDetails.class ).findFirst( callback );
  }

  public static OrderDetails findLast()
  {
    return Backendless.Data.of( OrderDetails.class ).findLast();
  }

  public static Future<OrderDetails> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<OrderDetails> future = new Future<OrderDetails>();
      Backendless.Data.of( OrderDetails.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<OrderDetails> callback )
  {
    Backendless.Data.of( OrderDetails.class ).findLast( callback );
  }

  public static BackendlessCollection<OrderDetails> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( OrderDetails.class ).find( query );
  }

  public static Future<BackendlessCollection<OrderDetails>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<OrderDetails>> future = new Future<BackendlessCollection<OrderDetails>>();
      Backendless.Data.of( OrderDetails.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<OrderDetails>> callback )
  {
    Backendless.Data.of( OrderDetails.class ).find( query, callback );
  }
}