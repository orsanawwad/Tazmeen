package net.crofis.tazmeen.Classes;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class FoodExtra
{
  private String name;
  private java.util.Date updated;
  private Double price;
  private java.util.Date created;
  private String ownerId;
  private String objectId;
  public String getName()
  {
    return name;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public Double getPrice()
  {
    return price;
  }

  public void setPrice( Double price )
  {
    this.price = price;
  }

  public java.util.Date getCreated()
  {
    return created;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

  public String getObjectId()
  {
    return objectId;
  }

                                                    
  public FoodExtra save()
  {
    return Backendless.Data.of( FoodExtra.class ).save( this );
  }

  public Future<FoodExtra> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<FoodExtra> future = new Future<FoodExtra>();
      Backendless.Data.of( FoodExtra.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<FoodExtra> callback )
  {
    Backendless.Data.of( FoodExtra.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( FoodExtra.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( FoodExtra.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( FoodExtra.class ).remove( this, callback );
  }

  public static FoodExtra findById( String id )
  {
    return Backendless.Data.of( FoodExtra.class ).findById( id );
  }

  public static Future<FoodExtra> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<FoodExtra> future = new Future<FoodExtra>();
      Backendless.Data.of( FoodExtra.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<FoodExtra> callback )
  {
    Backendless.Data.of( FoodExtra.class ).findById( id, callback );
  }

  public static FoodExtra findFirst()
  {
    return Backendless.Data.of( FoodExtra.class ).findFirst();
  }

  public static Future<FoodExtra> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<FoodExtra> future = new Future<FoodExtra>();
      Backendless.Data.of( FoodExtra.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<FoodExtra> callback )
  {
    Backendless.Data.of( FoodExtra.class ).findFirst( callback );
  }

  public static FoodExtra findLast()
  {
    return Backendless.Data.of( FoodExtra.class ).findLast();
  }

  public static Future<FoodExtra> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<FoodExtra> future = new Future<FoodExtra>();
      Backendless.Data.of( FoodExtra.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<FoodExtra> callback )
  {
    Backendless.Data.of( FoodExtra.class ).findLast( callback );
  }

  public static BackendlessCollection<FoodExtra> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( FoodExtra.class ).find( query );
  }

  public static Future<BackendlessCollection<FoodExtra>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<FoodExtra>> future = new Future<BackendlessCollection<FoodExtra>>();
      Backendless.Data.of( FoodExtra.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<FoodExtra>> callback )
  {
    Backendless.Data.of( FoodExtra.class ).find( query, callback );
  }
}