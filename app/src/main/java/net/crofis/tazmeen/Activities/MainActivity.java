package net.crofis.tazmeen.Activities;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.Messaging;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.geo.BackendlessGeoQuery;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.crofis.tazmeen.Adapters.SearchResultAdapter;
import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.LocationService;
import net.crofis.tazmeen.Services.Starter;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MainActivity";

    /** BroadcastReceiver used to receive updates from LocationService */
    private BroadcastReceiver receiver;

    /** GoogleMaps for manipulating the map and managing points on screen */
    private GoogleMap mMap;

    /** Add Current location */
    private Marker currentLocationMarker = null;

    /** Location location */
    private Location location;

    /** BranchMarkers */
    ArrayList<Marker> markers;

    /** Button to go to my current location */
    Button goToMyLocation;

    /** Menu, when user is logged in, used to navigate to profile, cart, work (if user have higher role than 0)*/
    private FloatingActionMenu fam;

    /** Fields to check for map movement */
    private Timer t;
    private LatLng ne;
    private LatLng sw;
    private Double sw1 = 0.0;
    private Double sw2 = 0.0;
    private Double ne1 = 0.0;
    private Double ne2 = 0.0;



    /**
     * FloatingActionButtons
     * */

    /** Log in fab, clicking it moves to sign in activity */
    private FloatingActionButton fab_sign_in;

    /** Cart fab, clicking it moves to cart activity */
    private FloatingActionButton fab_previous_purchase;

    /** Word fab, clicking it moves to work activity */
    private FloatingActionButton fab_work;

    /** Profile fab, clicking it moves to profile activity */
    private FloatingActionButton fab_profile;

    /**
     * End of FloatingActionButtons
     */

    private static Boolean hasStarted = false;

    /** EditText for searching and viewing closest branches */
    private EditText searchInput;
    private CardView searchCardView;
    private Boolean shouldSearch;
    private RecyclerView searchResults;
    private SearchResultAdapter searchAdapter;
    private TextView noRestaurantWarn;
    private Boolean activityOpened;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main);

        //Init ui
        if (!hasStarted) {
            hasStarted = true;
            initUI();
        }

        //TODO: Add custom icons for branches, opened, and closed
        //TODO: Add move to current location
        //TODO: Add map key info, red is closed, green is opened.
        //TODO: BUG: Fix location service on start where phone goes to sleep, add listener whenever google api client is connected

    }

    /**
     * Initialize UI Elements and register broadcast
     */
    private void initUI() {

        checkForUpdates();

        /** Find map fragment and set callback */
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //TODO: Extract this so the user can run it manually from button
        /** Init Broadcast reciever and implement onReceive to get updates from LocationService and set the current location icon*/
        initReceiver();

        /** Find goToMyLocation button */
        goToMyLocation = (Button)findViewById(R.id.main_activity_gotomylocation);

        goToMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMyLocation();
            }
        });

        /** Find each FloatingActionButton and set OnClickListeners*/
        fab_sign_in = (FloatingActionButton)findViewById(R.id.main_activity_fab_sign_in);

        fab_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this,LoginActivity.class), Starter.Constants.LOGIN_ACTIVITY_RESULT);
            }
        });

        fab_profile = (FloatingActionButton)findViewById(R.id.main_activity_fab_profile);

        fab_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MainActivity.this,ProfileActivity.class), 14111);
            }
        });

        fab_previous_purchase = (FloatingActionButton)findViewById(R.id.main_activity_fab_previous_purchase);

        fab_previous_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PreviousPurchase.class));
            }
        });

        fab_work = (FloatingActionButton)findViewById(R.id.main_activity_fab_work);

        if (fab_work != null) {
            fab_work.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Starter.Variables.user_role != null) {
                        if (Starter.Variables.user_role == Starter.Constants.USER_ROLE_WORKER) {
                            startActivity(new Intent(MainActivity.this,BranchManagerActivity.class));
                        }
                        else if (Starter.Variables.user_role == Starter.Constants.USER_ROLE_OWNER)
                        {
                            startActivity(new Intent(MainActivity.this,RestaurantOwnerActivity.class));
                        }
                    }
                }
            });
        }

        fam = (FloatingActionMenu)findViewById(R.id.main_activity_fam);
        /** End of finding FloatingActionButtons */

        /** Setup search for branches */
        setUpSearch();

        /**
         * Logic
         */

        /**
         * Check if user is already logged in and set UI according to each role, enable menu and assign color.
         */

        /** Start checking for user session */
        /** Disable sign in button and enable it once the app is sure that there are no active sessions */

        if(!isLocationEnabled(this)){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enable location services").setMessage("Tazmeen uses your location so you can get your orders delivered to you exactly where you are").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }).setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            }).setIcon(R.drawable.ic_location_dialog).create().show();
        }

        checkForUserSessionFromBackendLess();

//        checkPermissions();
    }

    //DO NOT INCLUDE THIS FROM STORE RELEASES
    private void checkForUpdates() {
        UpdateManager.register(this);
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }


    private void initReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Get location update
                location = intent.getExtras().getParcelable(Starter.Constants.LOCATION_SERVICE_MESSAGE);
                //Convert to LatLng
                LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
                //Check if map fragment loaded
                if(mMap != null)
                {
                    //check if currentLocationMarker ever initialized
                    if(currentLocationMarker == null)
                    {
                        BitmapDescriptor descriptor = getBitmapDescriptorFromResource(R.drawable.ic_current_location);
                        currentLocationMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("Current Location").icon(descriptor));
                        CameraUpdate cam_position = CameraUpdateFactory.newLatLng(latLng);
                        CameraUpdate cam_zoom = CameraUpdateFactory.zoomTo(14);
                        mMap.moveCamera(cam_position);
                        mMap.animateCamera(cam_zoom);
                        searchForBranch("");
                    }
                    //If it is already initialized update position
                    else
                    {
                        currentLocationMarker.setPosition(latLng);
                    }
                }
            }
        };
    }

    /** Move camera to my current location */
    private void goToMyLocation() {
        //Get location update
        if(location != null) {
            //Convert to LatLng
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            //Check if map fragment loaded
            if (mMap != null) {
                //check if currentLocationMarker ever initialized
                CameraUpdate cam_position = CameraUpdateFactory.newLatLng(latLng);
                CameraUpdate cam_zoom = CameraUpdateFactory.zoomTo(16);
                mMap.moveCamera(cam_position);
                mMap.animateCamera(cam_zoom);
            }
        }
    }

    /**
     * Fixed checkForUserSessionFromBackendLess so it doesn't check again once user exits the app and launch it again and changes the buttom from normal logged in to not logged in state
     */

    /** Check user from BackendLess */
    private void checkForUserSessionFromBackendLess() {
        fab_sign_in.setEnabled(false);
//        fab_sign_in.setShowProgressBackground(true);
//        fab_sign_in.setIndeterminate(true);
        Backendless.UserService.isValidLogin(new AsyncCallback<Boolean>() {
            @Override
            public void handleResponse(Boolean response) {
                /** If there is actually a user get its id */
                if (response)
                {
                    if(Backendless.UserService.CurrentUser() == null){
                        Log.i(TAG, "handleResponse: There is an existing user.");
                        String currentUserId = Backendless.UserService.loggedInUser();
                        if (!currentUserId.equals(""))
                        {
                            /** If there is an id get the user object from BackendLess */
                            Backendless.UserService.findById(currentUserId, new AsyncCallback<BackendlessUser>() {
                                @Override
                                public void handleResponse(BackendlessUser response) {
                                    /** Once found get the device id and save it again */
                                    /** After that set the user inside BackendLess CurrentUser UserService */
                                    response.setProperty("currentDevice", Messaging.DEVICE_ID);
                                    Backendless.UserService.update(response, new AsyncCallback<BackendlessUser>() {
                                        @Override
                                        public void handleResponse(BackendlessUser response) {
                                            Backendless.UserService.setCurrentUser(response);

                                            /** Get user's role and set it inside Starter.Variables (its like a shared preferences) */
                                            Starter.Variables.user_role = Integer.parseInt(response.getProperty("userRole").toString());

                                            Log.i(TAG, "handleResponse: User role is " + Starter.Variables.user_role);

                                            /** If everything went well, do this */
                                            if(Starter.Variables.user_role != null)
                                            {
                                                /** Hide login button, show menu, disable animation*/
//                                                fab_sign_in.setShowProgressBackground(false);
//                                                fab_sign_in.setIndeterminate(false);
//                                                float x = fab_sign_in.getX();
//                                                float y = fab_sign_in.getY();
//                                                fab_sign_in.hideProgress();
//                                                fab_sign_in.setX(x);
//                                                fab_sign_in.setY(y);
                                                fab_sign_in.hide(true);
                                                fam.showMenu(true);
                                                fam.setIconAnimated(false);

                                                /** Get user role then and get its colors accordingly from colors.xml to the colors[] then send it to setFabColors(color[]) */
                                                Integer user_role = Starter.Variables.user_role;
                                                int[] colors;
                                                if(user_role.equals(Starter.Constants.USER_ROLE_NORMAL))
                                                {
                                                    fam.removeMenuButton(fab_work);
                                                    colors = getResources().getIntArray(R.array.role_normal_colors);
                                                    setFabColors(colors);
                                                }
                                                else if(user_role.equals(Starter.Constants.USER_ROLE_WORKER))
                                                {
                                                    colors = getResources().getIntArray(R.array.role_worker_colors);
                                                    setFabColors(colors);
                                                }
                                                else if(user_role.equals(Starter.Constants.USER_ROLE_OWNER))
                                                {
                                                    colors = getResources().getIntArray(R.array.role_owner_colors);
                                                    setFabColors(colors);
                                                }
                                                else
                                                {
                                                    Log.e(TAG, "initUI: Something is not right at all, there is no user_role and it says that user exists, please check if current user have role");
                                                }
                                            }
                                            Log.i(TAG, "handleResponse: User currentDevice property updated and set as CurrentUser");
                                            Log.i(TAG, "handleResponse: Updated UI by user role");
                                        }

                                        @Override
                                        public void handleFault(BackendlessFault fault) {
                                            Log.e(TAG, "handleFault: " + fault.getMessage());
                                        }
                                    });
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    Log.e(TAG, "handleFault: Error finding user, Message: " + fault.getMessage() );
                                }
                            });
                        }
                    }
                    else{
                        transitionToRole();
                    }
                }
                else
                {
                    fab_sign_in.setEnabled(true);
//                    float x = fab_sign_in.getX();
//                    Log.d(TAG, "handleResponse: x: " + x);
//                    float y = fab_sign_in.getY();
//                    Log.d(TAG, "handleResponse: y: " + y);
//                    fab_sign_in.hideProgress();
//                    Log.d(TAG, "handleResponse: x: " + x);
//                    Log.d(TAG, "handleResponse: y: " + y);
//                    fab_sign_in.setX(x);
//                    fab_sign_in.setY(y);
                    Log.i(TAG, "handleResponse: No current user session found");
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: Error validating if user session exists" + fault.getMessage() );
            }
        });
    }

    /**
     * Set colors for the FloatingActionButtons and the FloatingActionMenu used to navigate most of the app, colors are used to differentiate between roles
     * @param colors, colors array used to change the fab color for each.
     * it adds all the fab colors into arrayList, then loop arount them, set the colors
     * 0 normal
     * 1 pressed
     * 2 ripple
     */
    private void setFabColors(int[] colors){
        ArrayList<FloatingActionButton> fabs = new ArrayList<>();
        if (fab_work != null) {
            fabs.add(fab_work);
        }
        fabs.add(fab_profile);
        fabs.add(fab_previous_purchase);
        fam.setMenuButtonColorNormal(colors[0]);
        fam.setMenuButtonColorPressed(colors[1]);
        fam.setMenuButtonColorRipple(colors[2]);
        for (int i = 0; i < fabs.size(); i++) {
            FloatingActionButton fab = fabs.get(i);
            fab.setColorNormal(colors[0]);
            fab.setColorPressed(colors[1]);
            fab.setColorRipple(colors[2]);
        }

        if(Starter.Variables.user_role.equals(Starter.Constants.USER_ROLE_NORMAL))
        {
            fam.getMenuIconView().setImageResource(R.drawable.ic_home);
        }
    }

    /**
     * Convert resource to bitmapDescription
     * @param resource the resource you want to convert
     * @return returns bitmapDescription object, used for markers
     * TODO: Use this when doing custom icon marks for branch marking
     */
    private BitmapDescriptor getBitmapDescriptorFromResource(int resource) {
        /**
         * This whole code snippet just takes a drawable and convert it to bitmap then bitmapDescription then set the marker
         * THEN set a unique icon to differentiate between any other marker to your current location.
         *
         * at least it works
         */
        Bitmap icon;
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), resource, null);
        if (drawable instanceof BitmapDrawable) {
            icon = ((BitmapDrawable)drawable).getBitmap();
        }
        else{
            icon = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(icon);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        }
        return BitmapDescriptorFactory.fromBitmap(icon);
    }

    /**
     * register receiver from LocationService on activity start
     */
    @Override
    protected void onStart() {
        super.onStart();
        if(receiver == null) initReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),new IntentFilter(Starter.Constants.LOCATION_SERVICE_RESULT));
        initUI();
    }

    /**
     * unregister receiver on activity stop
     */
    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    /**
     * Stop location update when user exits the app
     * TODO: keep location running whenever there is an ongoing order? then stop it when all orders are done
     */
    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, LocationService.class));
        unregisterManagers();
        Log.i(TAG, "onPause: LocationService paused");
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    /**
     * start location service when activity starts
     */
    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, LocationService.class));
        checkForCrashes();
        searchForBranch("");
        activityOpened = false;
        Log.i(TAG, "onResume: LocationService resumed");
    }

    /**
     * when maps loaded
     * @param googleMap save googleMap to global scope mMap and do set it up
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Assign googleMap to mMap field
        mMap = googleMap;

        //remove tilt rotate and compass
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setInfoWindowAdapter(new CustomMarkerInfo());

        Starter.Variables.bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
////                mMap.setOnCameraChangeListener(null);
//                marker.showInfoWindow();
//                Log.d(TAG, "onMarkerClick: " + marker.getTitle());
//                return true;
//            }
//        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (!marker.getTitle().equals("Current Location")) {
                    Intent intent = new Intent(MainActivity.this,BranchViewFoodActivity.class);
                    intent.putExtra("data",marker.getTitle());
                    startActivity(intent);
                }
            }
        });

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                if(searchInput != null && searchInput.hasFocus()){
                    searchInput.clearFocus();
                }

                Starter.Variables.bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

                //TEST
                LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
                ne = bounds.northeast;
                sw = bounds.southwest;
                if(t!=null){
                    t.purge();
                    t.cancel();
                }
                t = new Timer();
                t.schedule(new TimerTask() {
                    public void run() {
                        if(ne1 != ne.latitude && ne2 != ne.longitude && sw1 != sw.latitude && sw2 != sw.longitude){
                            ne1 = ne.latitude;
                            ne2 = ne.longitude;
                            sw1 = sw.latitude;
                            sw2 = sw.longitude;
                            Log.d("Tag","Refreshing data");
                            searchForRestaurants();
                            t.cancel();
                        }
                        else{
                            ne1 = ne.latitude;
                            ne2 = ne.longitude;
                            sw1 = sw.latitude;
                            sw2 = sw.longitude;}
                        t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
                    }
                }, 1000);
                //new DownloadJSON().execute();

//                searchForRestaurants();
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(searchInput != null && searchInput.hasFocus()){
                    searchInput.clearFocus();
                }
                if(fam.isOpened())
                {
                    fam.toggle(true);
                }
            }
        });
    }


    private void searchForRestaurants() {
        Log.d(TAG, "searchForRestaurants: Inside search");
        if(Starter.Variables.bounds != null)
        {
            Log.d(TAG, "searchForRestaurants: REQUESTING");
            LatLngBounds bound = Starter.Variables.bounds;
            BackendlessGeoQuery query = new BackendlessGeoQuery();

            /** The reason in the inputs are different between topLeft and bottomRight is that
             *  the query method required the top left coordinates of the maps so that means its northwest
             *  The LatLngBound only have northeast and southwest, so i took the points accordingly
             *  The northwest point is northeast latitude with southwest longitude
             *  And the southeast point is southwest latitude and northeast longitude */
            GeoPoint topLeft = new GeoPoint(bound.northeast.latitude,bound.southwest.longitude);
            GeoPoint bottomRight = new GeoPoint(bound.southwest.latitude,bound.northeast.longitude);

            /** Set the bound points */
            query.setSearchRectangle(topLeft,bottomRight);

            /** Add category as branches */
            query.addCategory("branches");

            /** Set include metadata */
            query.setIncludeMeta(true);

            /** Search for the points */
            Backendless.Geo.getPoints(query, new AsyncCallback<BackendlessCollection<GeoPoint>>() {
                @Override
                public void handleResponse(BackendlessCollection<GeoPoint> response) {
                    Log.d(TAG, "handleResponse() called with: " + "response = [" + response.toString() + "]");
                    Log.d(TAG, "handleResponse: Got response");
                    Log.d(TAG, "handleResponse: " + response.getData().size());
                    if(markers == null){markers = new ArrayList<Marker>();}
                    else {
                        for (Marker marker:
                             markers) {
                            marker.remove();
                        }
                    }
                    for (GeoPoint geopoint :
                            response.getData()) {

                        JSONObject json = new JSONObject();

                        try {
                            json.put("branchId",geopoint.getMetadata("branchId"));
                            json.put("imageUrl",geopoint.getMetadata("imageUrl"));
                            json.put("name",geopoint.getMetadata("name"));
                            json.put("isOpen",geopoint.getMetadata("isOpen"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "handleResponse: " + e.getMessage() );
                        }

                        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(geopoint.getLatitude(),geopoint.getLongitude())).title(json.toString()));
                        markers.add(marker);
                    }
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "handleFault: " + fault.getMessage() );
                }
            });
        }
    }


    /** Custom view for marker info window */
    private class CustomMarkerInfo implements GoogleMap.InfoWindowAdapter {

        private View v;

        public CustomMarkerInfo() {
            v = getLayoutInflater().inflate(R.layout.info_branch_marker_map, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(final Marker marker) {
            if(marker.getTitle().equals("Current Location")) return null;
            JSONObject json = null;
            try {
                Log.d(TAG, "getInfoContents: " + marker.getTitle());
                json = new JSONObject(marker.getTitle());
                ((TextView)v.findViewById(R.id.branch_marker_map_info_name)).setText(json.get("name").toString());

                if(json.get("isOpen").toString().equals("true"))
                {
                    ((View)v.findViewById(R.id.branch_marker_map_info_status)).setBackgroundColor(Color.parseColor("#64dd17"));
                }
                else
                {
                    ((View)v.findViewById(R.id.branch_marker_map_info_status)).setBackgroundColor(Color.parseColor("#ff0000"));
                }
                Glide.with(MainActivity.this).load(json.getString("imageUrl")).asBitmap().fitCenter().into(((ImageView)v.findViewById(R.id.branch_marker_map_info_image)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return v;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if(resultCode == RESULT_OK)
        {
            Log.i(TAG, "onActivityResult: Inside if result ok");
            if(requestCode == Starter.Constants.LOGIN_ACTIVITY_RESULT)
            {
                Log.i(TAG, "onActivityResult: Inside if request code is same as login activity");
                if(Backendless.UserService.CurrentUser() != null)
                {
                    Log.i(TAG, "onActivityResult: Inside user not null");
                    transitionToRole();
                }
            }
            else if(requestCode == 14111)
            {
                recreate();
                startActivityForResult(new Intent(MainActivity.this,LoginActivity.class), Starter.Constants.LOGIN_ACTIVITY_RESULT);
            }
        }
    }

    private void transitionToRole() {
        /** If everything went well, do this */
        if(Starter.Variables.user_role != null)
        {
            /** Hide login button, show menu, disable animation*/
            fab_sign_in.hide(true);
            fam.showMenu(true);
            fam.setIconAnimated(false);

            /** Get user role then and get its colors accordingly from colors.xml to the colors[] then send it to setFabColors(color[]) */
            Integer user_role = Starter.Variables.user_role;
            int[] colors;
            if(user_role.equals(Starter.Constants.USER_ROLE_NORMAL))
            {
                if (fab_work != null) {
                    fam.removeMenuButton(fab_work);
                }
                colors = getResources().getIntArray(R.array.role_normal_colors);
                setFabColors(colors);
            }
            else if(user_role.equals(Starter.Constants.USER_ROLE_WORKER))
            {
                colors = getResources().getIntArray(R.array.role_worker_colors);
                setFabColors(colors);
            }
            else if(user_role.equals(Starter.Constants.USER_ROLE_OWNER))
            {
                colors = getResources().getIntArray(R.array.role_owner_colors);
                setFabColors(colors);
            }
            else
            {
                Log.e(TAG, "initUI: Something is not right at all, there is no user_role and it says that user exists, please check if current user have role");
            }
        }
        Log.i(TAG, "handleResponse: User currentDevice property updated and set as CurrentUser");
        Log.i(TAG, "handleResponse: Updated UI by user role");
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    /** Setup search function */
    public void setUpSearch(){

        /** Find the view */
        searchInput = (EditText) findViewById(R.id.main_activity_search_edittext);

        searchCardView = (CardView) findViewById(R.id.main_activity_search_cardview);

        searchResults = (RecyclerView) findViewById(R.id.main_activity_search_results);

        noRestaurantWarn = (TextView) findViewById(R.id.main_activity_search_warn);

        /** RecyclerView setup */
        searchAdapter = new SearchResultAdapter(this);

        searchResults.setLayoutManager(new LinearLayoutManager(this));

        searchResults.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        searchResults.setAdapter(searchAdapter);

        searchResults.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), searchResults, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //TODO: run branch page from here

                if (!activityOpened) {
                    activityOpened = true;
                    Branch selected = searchAdapter.getBranches().get(position);

                    JSONObject json = new JSONObject();
                    try {
                        json.put("branchId",selected.getObjectId());
                        json.put("imageUrl",selected.getImageUrl());
                        json.put("name",selected.getRestaurantName());
                        json.put("isOpen",selected.getIsOpen().toString());
                        Intent intent = new Intent(MainActivity.this,BranchViewFoodActivity.class);
                        intent.putExtra("data",json.toString());
                        startActivity(intent);
                        Log.d(TAG, "onClick: HOW MANY CLICKS FOR REAL NOW?!?!?!");
                    } catch (JSONException e) {
                        Log.e(TAG, "onClick: " + e.getMessage());
                    }
                }


            }

            @Override
            public void onLongClick(View view, int position) {
                //no need to implement
            }
        }));

        /***/
//        searchForBranch("");

        searchInput.setHint("Looking for restaurants around you...");

        /** Set focusable listener to trigger flag that a search can be called and view results */
        searchInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            Float startingElevation = null;
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(startingElevation == null)
                    {
                        startingElevation = searchCardView.getElevation();
                    }
                }
                if(hasFocus) {
                    searchInput.setHint("Search here...");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Animations.changeElevation(searchCardView,searchCardView.getElevation(),((8/2)*6));
                    }
                    searchResults.setVisibility(View.VISIBLE);
                    shouldSearch = true;
                }
                else
                {
                    noRestaurantWarn.setVisibility(View.GONE);
                    searchForBranch("");
                    searchInput.setHint("Looking for restaurants around you...");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Animations.changeElevation(searchCardView,searchCardView.getElevation(),8f);
                    }
                    searchResults.setVisibility(View.GONE);
                    searchInput.setText("");
                    shouldSearch = false;
                    InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            private Timer timer=new Timer();
            private final long DELAY = 500; // milliseconds

            @Override
            public void afterTextChanged(final Editable s) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                searchForBranch(s.toString());
                            }
                        }, DELAY);
            }
        });
    }

    private void searchForBranch(final String search){
        if (Starter.Variables.location != null) {
            double myLatitude = Starter.Variables.location.getLatitude();
            double myLongitude = Starter.Variables.location.getLongitude();
            String query = null;
            if(!search.equals("")) {
                query = "distance( "+myLatitude+", "+myLongitude+", location.latitude, location.longitude ) < km(5) and restaurantName LIKE '%" +search+ "%'";
            }
            else {
                query = "distance( "+myLatitude+", "+myLongitude+", location.latitude, location.longitude ) < km(5)";
            }
//            String whereClause = String.format( query, myLatitude, myLongitude );

            BackendlessDataQuery dataQuery = new BackendlessDataQuery( query );
            QueryOptions queryOptions = new QueryOptions();
            queryOptions.addRelated( "location" );
            dataQuery.setQueryOptions( queryOptions );

            Backendless.Data.of( Branch.class ).find(dataQuery, new AsyncCallback<BackendlessCollection<Branch>>() {
                @Override
                public void handleResponse(BackendlessCollection<Branch> response) {
                    if(searchAdapter != null && response.getData().size() > 0)
                    {
                        if(!searchInput.hasFocus() && search.equals(""))
                        {
                            String isOrAre = null;
                            String restaurantOrRestaurants = null;
                            if(response.getData().size() == 1){
                                isOrAre = "is";
                                restaurantOrRestaurants = "restaurant";
                            }
                            else {
                                isOrAre = "are";
                                restaurantOrRestaurants = "restaurants";
                            }
                            searchInput.setHint("There "+isOrAre+" " + response.getData().size() + " "+restaurantOrRestaurants+" around you");
                        }
                        if (searchInput.hasFocus()) {
                            noRestaurantWarn.setVisibility(View.GONE);
                        }
                        searchAdapter.setBranches((ArrayList<Branch>) response.getData());
                    }
                    else {
                        if(!searchInput.hasFocus() && search.equals(""))
                        {
                            searchInput.setHint("No restaurants around you...");
                        }
                        if (searchInput.hasFocus()) {
                            noRestaurantWarn.setVisibility(View.VISIBLE);
                        }
                        searchAdapter.setBranches((ArrayList<Branch>) response.getData());
                    }
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "handleFault: " + fault);
                    Toast.makeText(MainActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    static class Animations{
        public static void changeElevation(View v,float start,float end){
            ObjectAnimator anim = ObjectAnimator.ofFloat(v, "elevation", start, end);
            anim.setInterpolator(new FastOutSlowInInterpolator());
            anim.setDuration(225);
            anim.start();
        }

//        public static void changeCardViewSize(View v,int start,int end)
//        {
//            ObjectAnimator anim = ObjectAnimator.ofInt(v,"height",start,end);
//            anim.setInterpolator(new FastOutSlowInInterpolator());
//            anim.setDuration(225);
//            anim.start();
//        }
    }
    class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{
                android.R.attr.listDivider
        };

        public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

        public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

        private Drawable mDivider;

        private int mOrientation;

        public DividerItemDecoration(Context context, int orientation) {
            final TypedArray a = context.obtainStyledAttributes(ATTRS);
            mDivider = a.getDrawable(0);
            a.recycle();
            setOrientation(orientation);
        }

        public void setOrientation(int orientation) {
            if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
                throw new IllegalArgumentException("invalid orientation");
            }
            mOrientation = orientation;
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            if (mOrientation == VERTICAL_LIST) {
                drawVertical(c, parent);
            } else {
                drawHorizontal(c, parent);
            }
        }

        public void drawVertical(Canvas c, RecyclerView parent) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin;
                final int bottom = top + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        public void drawHorizontal(Canvas c, RecyclerView parent) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int left = child.getRight() + params.rightMargin;
                final int right = left + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            if (mOrientation == VERTICAL_LIST) {
                outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
            } else {
                outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
            }
        }
    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private MainActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final MainActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onBackPressed() {
        if(searchInput.hasFocus()){
            searchInput.clearFocus();
        } else {
            super.onBackPressed();
        }
    }
}


/**
 * Junk Code here
 * AKA the graveyard
 */


///**
// * Test
// * */
//if(Starter.Variables.user_role != null)
//        {
//        fab_sign_in.hide(true);
//        fam.showMenu(true);
//        }
//
//        /**
//         * UI Customization for each role
//         */
//
//        /**
//         * If user role is 0 (normal user) hide work
//         * Check user role and add the correspondence colors for fab buttons
//         */
//        if(Starter.Variables.user_role != null)
//        {
//        Integer user_role = Starter.Variables.user_role;
//        fab_sign_in.setVisibility(View.INVISIBLE);
//        fam.setVisibility(View.VISIBLE);
//        int[] colors;
//        if(user_role.equals(Starter.Constants.USER_ROLE_NORMAL))
//        {
//        fab_work.setVisibility(View.INVISIBLE);
//        colors = getResources().getIntArray(R.array.role_normal_colors);
//        setFabColors(colors);
//        }
//        else if(user_role.equals(Starter.Constants.USER_ROLE_WORKER))
//        {
//        colors = getResources().getIntArray(R.array.role_worker_colors);
//        setFabColors(colors);
//        }
//        else if(user_role.equals(Starter.Constants.USER_ROLE_OWNER))
//        {
//        colors = getResources().getIntArray(R.array.role_owner_colors);
//        setFabColors(colors);
//        }
//        else
//        {
//        Log.e(TAG, "initUI: Something is not right at all, there is no user_role and it says that user exists, please check if current user have role");
//        }
//        }


// RIP
///** If user is logged in hide Login button and display fab menu */
////TODO: Fix this, re 'calibrate the code' so the floating action buttons show up right
//if(Starter.Variables.user_role != null)
//        {
//        fab_sign_in.hide(true);
//        fam.showMenu(true);
//        }
//
//        /**
//         * UI Customization for each role
//         */
//
//        /**
//         * If user role is 0 (normal user) hide work
//         * Check user role and add the correspondence colors for fab buttons
//         */
//        if(Starter.Variables.user_role != null)
//        {
//        Integer user_role = Starter.Variables.user_role;
//        fab_sign_in.setVisibility(View.INVISIBLE);
//        fam.setVisibility(View.VISIBLE);
//        int[] colors;
//        if(user_role.equals(Starter.Constants.USER_ROLE_NORMAL))
//        {
//        fab_work.setVisibility(View.INVISIBLE);
//        colors = getResources().getIntArray(R.array.role_normal_colors);
//        setFabColors(colors);
//        }
//        else if(user_role.equals(Starter.Constants.USER_ROLE_WORKER))
//        {
//        colors = getResources().getIntArray(R.array.role_worker_colors);
//        setFabColors(colors);
//        }
//        else if(user_role.equals(Starter.Constants.USER_ROLE_OWNER))
//        {
//        colors = getResources().getIntArray(R.array.role_owner_colors);
//        setFabColors(colors);
//        }
//        else
//        {
//        Log.e(TAG, "initUI: Something is not right at all, there is no user_role and it says that user exists, please check if current user have role");
//        }
//        }


//
//        Integer user_role = Starter.Variables.user_role;
//        int[] colors;
//        fab_sign_in.hide(true);
//        fam.setVisibility(View.VISIBLE);
//        if(user_role.equals(Starter.Constants.USER_ROLE_NORMAL))
//        {
//            fab_work.setVisibility(View.INVISIBLE);
//            colors = getResources().getIntArray(R.array.role_normal_colors);
//            setFabColors(colors);
//        }
//        else if(user_role.equals(Starter.Constants.USER_ROLE_WORKER))
//        {
//            colors = getResources().getIntArray(R.array.role_worker_colors);
//            setFabColors(colors);
//        }
//        else if(user_role.equals(Starter.Constants.USER_ROLE_OWNER))
//        {
//            colors = getResources().getIntArray(R.array.role_owner_colors);
//            setFabColors(colors);
//        }
//        else if(Backendless.UserService.CurrentUser() == null)
//        {
//            fam.hideMenu(true);
//            fab_sign_in.show(true);
//        }
//        else
//        {
//            Log.e(TAG, "initUI: Something is not right at all, there is no user_role and it says that user exists, please check if current user have role");
//        }


//    class GlideInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
//        private final Map<Marker, Bitmap> images = new HashMap<>();
//        private final Map<Marker, Target<Bitmap>> targets = new HashMap<>();
//
//        /** initiates loading the info window and makes sure the new image is used in case it changed */
//        public void showInfoWindow(Marker marker) {
//            Glide.clear(targets.get(marker)); // will do images.remove(marker) too
//            marker.showInfoWindow(); // indirectly calls getInfoContents which will return null and start Glide load
//        }
//        /** use this to discard a marker to make sure all resources are freed and not leaked */
//        public void remove(Marker marker) {
//            images.remove(marker);
//            Glide.clear(targets.remove(marker));
//            marker.remove();
//        }
//        public View getInfoContents(Marker marker) {
//            Bitmap image = images.get(marker);
//            if (image == null) {
//                Glide.with(MainActivity.this).load(url).asBitmap().dontAnimate().into(getTarget(marker));
//                return null; // or something indicating loading
//            } else {
//                ImageView iv = null; // however you create it
//                iv.setImageBitmap(image);
//                return iv;
//            }
//        }
//        public View getInfoWindow(Marker marker) {
//            return null;
//        }
//        private Target<Bitmap> getTarget(Marker marker) {
//            Target<Bitmap> target = targets.get(marker);
//            if (target == null) {
//                target = new InfoTarget(marker);
//            }
//            return target;
//        }
//        private class InfoTarget extends SimpleTarget<Bitmap> {
//            Marker marker;
//            InfoTarget(Marker marker) {
//                super(markerWidth, markerHeight); // otherwise Glide will load original sized bitmap which is huge
//                this.marker = marker;
//            }
//            @Override public void onLoadCleared(Drawable placeholder) {
//                images.remove(marker); // clean up previous image, it became invalid
//                // don't call marker.showInfoWindow() to update because this is most likely called from Glide.into()
//            }
//            @Override public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                // this prevents recursion, because Glide load only starts if image == null in getInfoContents
//                images.put(marker, resource);
//                // tell the maps API it can try to call getInfoContents again, this time finding the loaded image
//                marker.showInfoWindow();
//            }
//        }
//    }

//                URL url = new URL(json.getString("imageUrl"));
//                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());

//                ((ImageView)v.findViewById(R.id.branch_marker_map_info_image)).setImageBitmap(resource);

//                new SimpleTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                        ((ImageView)v.findViewById(R.id.branch_marker_map_info_image)).setImageBitmap(resource);
//                        if(marker.isInfoWindowShown())
//                        {
//                            marker.hideInfoWindow();
//                            if(!marker.isInfoWindowShown())
//                            {
//                                marker.showInfoWindow();
//                            }
//                        }
//                    }
//                });