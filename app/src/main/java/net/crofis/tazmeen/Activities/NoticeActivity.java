package net.crofis.tazmeen.Activities;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;

import net.crofis.tazmeen.R;

public class NoticeActivity extends AppCompatActivity {

    TextView notice;
    Button agree;

    String noticeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        if(getPreferences(MODE_PRIVATE).getBoolean("agreedToNotice",false))
        {
            startActivity(new Intent(NoticeActivity.this,MainActivity.class));
            finish();
        }

        getSupportActionBar().setTitle("Notice");

        notice = (TextView)findViewById(R.id.notice_activity_notice_text);
        agree = (Button)findViewById(R.id.notice_activity_agree);

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions();
            }
        });

    }

    private void requestPermissions() {
        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(this,
                new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        getPreferences(MODE_PRIVATE).edit().putBoolean("agreedToNotice",true ).commit();
                        startActivity(new Intent(NoticeActivity.this,MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onDenied(String permission) {
                        Toast.makeText(NoticeActivity.this, "Please kindly allow all of the required permissions for the app to function as intended", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
    }
}


/**
 * Graveyard
 * */

//        if(!PermissionsManager.hasAllPermissions(this,new String[]{
//                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.GET_ACCOUNTS,
//                Manifest.permission.CALL_PHONE,
//                Manifest.permission.VIBRATE,
//                Manifest.permission.CAMERA}))
//        {
//            requestPermissions();
//        }