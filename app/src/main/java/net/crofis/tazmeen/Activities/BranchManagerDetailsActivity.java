package net.crofis.tazmeen.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.github.clans.fab.FloatingActionButton;

import net.crofis.tazmeen.Adapters.BranchOrderDetailsWorkerAdapter;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;

public class BranchManagerDetailsActivity extends AppCompatActivity {

    private static final String TAG = "BMDActivity";

    RecyclerView recyclerView;

    Bundle extras;

    String orderId;

    BranchOrderDetailsWorkerAdapter adapter;

    FloatingActionButton accept,deny,done;

    Order order;

    String deviceId;

    Boolean flagIsNotSeller;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_manager_details);

        extras = getIntent().getExtras();

        orderId = extras.getString("objectId");

        flagIsNotSeller = extras.getBoolean("flagIsNotSeller");

        Log.d(TAG, "onCreate: " + flagIsNotSeller);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Details");

        recyclerView = (RecyclerView)findViewById(R.id.branch_manager_details_activity_details);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        accept = (FloatingActionButton)findViewById(R.id.branch_manager_details_activity_accept);

        deny = (FloatingActionButton)findViewById(R.id.branch_manager_details_activity_deny);

        done = (FloatingActionButton)findViewById(R.id.branch_manager_details_activity_done);

        if(flagIsNotSeller)
        {
            accept.setVisibility(View.INVISIBLE);
            deny.setVisibility(View.INVISIBLE);
            done.setVisibility(View.INVISIBLE);
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.setStatus("accepted");
                order.saveAsync(new AsyncCallback<Order>() {
                    @Override
                    public void handleResponse(Order response) {
                        accept.hide(true);
                        deny.hide(true);
                        done.show(true);
                        Starter.sendPush(deviceId,"Your order is being prepared","Your order is being prepared","Youll get another notificiation when it's done",Starter.Constants.NOTIFICATION_MESSAGE_ACCEPTED);
                        Toast.makeText(BranchManagerDetailsActivity.this, "Accepted", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e(TAG, "handleFault: " + fault.getMessage());
                    }
                });
            }
        });

        deny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.setStatus("denied");
                order.saveAsync(new AsyncCallback<Order>() {
                    @Override
                    public void handleResponse(Order response) {
                        accept.hide(true);
                        done.hide(true);
                        deny.setEnabled(false);
                        Starter.sendPush(deviceId,"Your order has been denied, try again later","Your order has been denied.","Try again later.",Starter.Constants.NOTIFICATION_MESSAGE_DENIED);
                        Toast.makeText(BranchManagerDetailsActivity.this, "Denied", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e(TAG, "handleFault: " + fault.getMessage());
                    }
                });
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.setStatus("done");
                order.saveAsync(new AsyncCallback<Order>() {
                    @Override
                    public void handleResponse(Order response) {
                        accept.hide(true);
                        deny.hide(true);
                        done.show(true);
                        done.setEnabled(false);
                        Starter.sendPush(deviceId,"Your order is on it's way!","Your order is done.","Your order is done and is now being delivered.",Starter.Constants.NOTIFICATION_MESSAGE_DONE);
                        Toast.makeText(BranchManagerDetailsActivity.this, "Done", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e(TAG, "handleFault: " + fault.getMessage());
                    }
                });
            }
        });

        String whereClause = "objectId = '" + orderId + "'";
        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
        Backendless.Persistence.of(Order.class).find(query, new AsyncCallback<BackendlessCollection<Order>>() {
            @Override
            public void handleResponse(BackendlessCollection<Order> response) {
                if(response.getData().size() == 1)
                {
                    order = response.getData().get(0);
                    adapter = new BranchOrderDetailsWorkerAdapter(BranchManagerDetailsActivity.this,response.getData().get(0));
                    recyclerView.setAdapter(adapter);

                    String userId = order.getOwnerId();

                    Backendless.Persistence.of(BackendlessUser.class).findById(userId, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {

                            deviceId = response.getProperty("currentDevice").toString();

                            Log.d(TAG, "handleResponse: userDeviceId" + deviceId);

                            if(deviceId.equals("")) deviceId ="USER IS OFFLINE";

                            if (!flagIsNotSeller) {
                                if(order.getStatus().equals("pending"))
                                {
                                    accept.show(true);
                                    deny.show(true);
                                    done.hide(true);
                                }
                                else if(order.getStatus().equals("accepted"))
                                {
                                    accept.hide(true);
                                    deny.hide(true);
                                    done.show(true);
                                }
                                else if(order.getStatus().equals("denied"))
                                {
                                    accept.hide(true);
                                    deny.show(true);
                                    done.hide(true);
                                    deny.setEnabled(false);
                                }
                                else if(order.getStatus().equals("done"))
                                {
                                    accept.hide(true);
                                    deny.hide(true);
                                    done.show(true);
                                    done.setEnabled(false);
                                }
                            }
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }
}
