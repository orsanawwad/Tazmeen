package net.crofis.tazmeen.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.bumptech.glide.Glide;

import net.crofis.tazmeen.Adapters.BranchOrderWorkerAdapter;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PreviousPurchase extends AppCompatActivity {

    private static final String TAG = "PreviousPurchase";

    RecyclerView orderList;

    BranchOrderWorkerAdapter adapter;

    TextView noOrderWarn;

    ArrayList<Order> ordersB;

    ImageView background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_purchase);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Previous purchases");

        orderList = (RecyclerView)findViewById(R.id.previous_purchase_activity_list);

        orderList.setLayoutManager(new LinearLayoutManager(this));

        noOrderWarn = (TextView)findViewById(R.id.previous_purchase_activity_noorderswarn);

        background = (ImageView)findViewById(R.id.previous_purchase_activity_background);

        Glide.with(this).load(R.drawable.background_food).centerCrop().into(background);

        getOrders();
    }

    public void getOrders() {
        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
        Backendless.Persistence.of(Order.class).find(query, new AsyncCallback<BackendlessCollection<Order>>() {
            @Override
            public void handleResponse(BackendlessCollection<Order> response) {
                if (response.getData().size() > 0) {
                    ordersB = (ArrayList<Order>)response.getData();
                    Collections.sort(ordersB, new Comparator<Order>() {
                        @Override
                        public int compare(Order lhs, Order rhs) {
                            return lhs.getCreated().compareTo(rhs.getCreated());
                        }
                    });
                    Collections.reverse(ordersB);
                    adapter = new BranchOrderWorkerAdapter(PreviousPurchase.this,ordersB,null,true);
                    orderList.setAdapter(adapter);
                    noOrderWarn.setVisibility(View.INVISIBLE);
                } else {
                    noOrderWarn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders_filter,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            //TODO: add filter
            case R.id.orders_filter_menu_all:
                getOrders();
                noOrderWarn.setVisibility(View.INVISIBLE);
                break;
            case R.id.orders_filter_menu_pending:
                filterList("pending");
                break;
            case R.id.orders_filter_menu_accepted:
                filterList("accepted");
                break;
            case R.id.orders_filter_menu_done:
                filterList("done");
                break;
            case R.id.orders_filter_menu_canceled:
                filterList("denied");
                break;
        }
        return true;
    }

    private void filterList(String status){
        ArrayList<Order> filter = new ArrayList<>();

        if (ordersB != null) {
            for (Order order23 :
                    ordersB) {
                if(order23.getStatus().equals(status))
                {
                    filter.add(order23);
                }
            }
        }
        if(filter.size() != 0)
        {
            adapter.setOrders(filter);
        }
        else
        {
            if(status.equals("done")) status = "completed";
            else if(status.equals("denied")) status = "canceled";
            else if(status.equals("accepted")) status = "ongoing";
            Toast.makeText(PreviousPurchase.this, "No " + status + " orders", Toast.LENGTH_SHORT).show();
        }
    }
}
