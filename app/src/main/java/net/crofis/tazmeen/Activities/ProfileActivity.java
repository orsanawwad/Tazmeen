package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.bumptech.glide.Glide;

import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.LoadingDialog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";

    /** The user to get the data from */
    BackendlessUser user;

    /** FirstName LastName textView to display person's name */
    TextView firstName,lastName;

    /** EditText CurrentPassword NewPassword ConfirmPassword for changing password */
    TextInputEditText newPassword,confirmPassword;

    /** Profile Image of the user */
    ImageView image;

    /** Button submitRestaurant for user to publish his own restaurant */
    CardView submitRestaurant;

    /** Button change password to change user's password */
    CardView changePassword;

    /** TextView where the user work at if his role is work */
    TextView workRestaurant;

    /** RelativeLayout for the own a restaurant question to hide from user if he's a manager */
    RelativeLayout ownARestaurantLayout;

    /** Restaurant name */
    String restaurantName;

    /** Logout button */
    CardView logout;

    /** The branch itself */
    Branch mBranch;

    LoadingDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Profile");

        /** Get the user */
        user = Backendless.UserService.CurrentUser();

        /** Find the views */
        image = (ImageView)findViewById(R.id.profile_activity_image);

        firstName = (TextView)findViewById(R.id.profile_activity_firstname);

        lastName = (TextView)findViewById(R.id.profile_activity_lastname);

        newPassword = (TextInputEditText)findViewById(R.id.profile_activity_newpassword);

        confirmPassword = (TextInputEditText)findViewById(R.id.profile_activity_confirmpassword);

        submitRestaurant = (CardView)findViewById(R.id.profile_activity_submitrestaurant);

        workRestaurant = (TextView)findViewById(R.id.profile_activity_youcurrentlyworkat);

        ownARestaurantLayout = (RelativeLayout)findViewById(R.id.profile_activity_layout_question_ownrestaurant);

        changePassword = (CardView)findViewById(R.id.profile_activity_changepassword);

        logout = (CardView)findViewById(R.id.profile_activity_logout);

        /** Logic */

        String name = user.getProperty("firstName").toString() + " " + user.getProperty("lastName");

        firstName.setText(name);
        lastName.setText(user.getProperty("userName").toString());

        int role = Integer.parseInt(user.getProperty("userRole").toString());

        if(role == Starter.Constants.USER_ROLE_NORMAL){
            workRestaurant.setVisibility(View.INVISIBLE);
        }
        else if(role == Starter.Constants.USER_ROLE_OWNER)
        {
            ownARestaurantLayout.setVisibility(View.INVISIBLE);
            workRestaurant.setText("Currently the manager of: ");
            getRestaurantName();
        }
        else if(role == Starter.Constants.USER_ROLE_WORKER)
        {
//            workRestaurant.setVisibility(View.INVISIBLE);
            ownARestaurantLayout.setVisibility(View.INVISIBLE);
            workRestaurant.setText("Currently work at: ");
            /**
             * Bug each time i get a branch the location meta data is removed
             * */
            getBranchItself();
        }

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = new LoadingDialog(ProfileActivity.this,"Changing password...");

                dialog.show();

                String pass1 = newPassword.getText().toString();
                String pass2 = confirmPassword.getText().toString();

                dialog.getMessage().setText("Checking for data...");
                if(pass1.equals(pass2) && !pass1.isEmpty() && !pass2.isEmpty())
                {
                    user.setPassword(pass1);

                    dialog.getMessage().setText("Updating user...");
                    Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            Toast.makeText(ProfileActivity.this, "Password Changed", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            dialog.dismiss();
                            Toast.makeText(ProfileActivity.this, "Something went wrong, try again later.", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                        }
                    });
                }
                else
                {
                    dialog.dismiss();
                    Toast.makeText(ProfileActivity.this, "Passwords do not match or empty fields, try again", Toast.LENGTH_SHORT).show();
                }
            }
        });

        submitRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProfileActivity.this, SubmitRestaurant.class),12312);
            }
        });

        setImage();


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = new LoadingDialog(ProfileActivity.this,"Logging out...");

                dialog.show();

                user.setProperty("currentDevice","");
                dialog.getMessage().setText("Updating user...");
                Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        dialog.getMessage().setText("Logging out from tazmeen...");
                        Backendless.UserService.logout(new AsyncCallback<Void>() {
                            @Override
                            public void handleResponse(Void response) {
                                dialog.dismiss();
                                setResult(RESULT_OK);
                                finish();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.e(TAG, "handleFault: " + fault.getMessage());
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e(TAG, "handleFault: " + fault.getMessage());
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private void setImage() {
        String email = user.getEmail().trim().toLowerCase();
        MessageDigest m;
        try {
            m = MessageDigest.getInstance("MD5");
            m.update(email.getBytes());

            byte emailByte[] = m.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < emailByte.length; i++) {
                sb.append(Integer.toString((emailByte[i] & 0xff) + 0x100, 16).substring(1));
            }

            String url = "http://www.gravatar.com/avatar/" + sb.toString() + "?d=http://i.imgur.com/DNfcpkA.png";

            Glide.with(this).load(url).asBitmap().into(image);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://en.gravatar.com/"));
                    startActivity(browserIntent);
                }
            });

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void getRestaurantName() {
        String whereClause = "ownerId = '" + user.getObjectId()+"'";
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if(response.getData().size() == 1){
                    restaurantName = response.getData().get(0).getName();

                    workRestaurant.setText(workRestaurant.getText().toString() + restaurantName);
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    private void getBranchItself(){
        HashMap branch = null;
        try {
            branch = (HashMap) user.getProperty("workBranch");
        } catch (Exception e) {
            Log.e(TAG, "getBranchItself: " + e.getMessage());
            branch = new HashMap();
            branch.put("objectId",Backendless.UserService.CurrentUser().getProperty("workId"));
        }
        Backendless.Persistence.mapTableToClass("Branch",Branch.class);
        Backendless.Persistence.of(Branch.class).findById(branch.get("objectId").toString(),2, new AsyncCallback<Branch>() {
            @Override
            public void handleResponse(Branch response) {
                mBranch = response;
                getRestaurantNameFromBranch();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    private void getRestaurantNameFromBranch(){
        String whereClause = "branches.objectId = '" + mBranch.getObjectId() + "'";
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if(response.getData().size() == 1){
                    restaurantName = response.getData().get(0).getName();
                    workRestaurant.setText(workRestaurant.getText().toString() + restaurantName);
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == 12312)
        {
            recreate();
        }
    }
}
