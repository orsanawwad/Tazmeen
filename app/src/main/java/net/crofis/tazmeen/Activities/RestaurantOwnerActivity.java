package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import net.crofis.tazmeen.R;

public class RestaurantOwnerActivity extends AppCompatActivity {

    Button goToBranches,goToFoods,goToFoodExtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_owner);

        initUI();
    }

    private void initUI(){

        getSupportActionBar().setTitle("Owner");

        goToBranches = (Button)findViewById(R.id.restaurant_activity_owner_branches);

        goToFoods = (Button)findViewById(R.id.restaurant_activity_owner_foods);

        goToFoodExtras = (Button)findViewById(R.id.restaurant_activity_owner_foodextras);

        goToBranches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RestaurantOwnerActivity.this,OwnerViewBranchesActivity.class));
            }
        });

        goToFoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RestaurantOwnerActivity.this,OwnerViewFoodsActivity.class));
            }
        });

        goToFoodExtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RestaurantOwnerActivity.this,OwnerViewFoodExtrasActivity.class));
            }
        });
    }
}
