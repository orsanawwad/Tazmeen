package net.crofis.tazmeen.Activities;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.Persistence;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.geo.BackendlessGeoQuery;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import net.crofis.tazmeen.Adapters.BranchOrderWorkerAdapter;
import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class BranchManagerActivity extends AppCompatActivity {

    private static final String TAG = "BranchManagerActivity";

    RecyclerView orders;

    BackendlessUser user;

    BranchOrderWorkerAdapter adapter;

    HashMap branch;

    Branch mBranch;

    FragmentManager manager;

    FloatingActionButton setOpened,setClosed;

    FloatingActionMenu menu;

    GeoPoint point;

    TextView noOrderWarn;

    ArrayList<Order> ordersB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_manager);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Manager");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /** Find and init the fields and views */

        user = Backendless.UserService.CurrentUser();

        try {
            branch = (HashMap) user.getProperty("workBranch");
        } catch (Exception e) {
            Log.e(TAG, "getBranchItself: " + e.getMessage());
            branch = new HashMap();
            branch.put("objectId",Backendless.UserService.CurrentUser().getProperty("workId"));
        }

        orders = (RecyclerView)findViewById(R.id.branch_manager_activity_allorders);

        menu = (FloatingActionMenu)findViewById(R.id.branch_manager_activity_control);

        setOpened = (FloatingActionButton)findViewById(R.id.branch_manager_activity_control_setopened);

        setClosed = (FloatingActionButton)findViewById(R.id.branch_manager_activity_control_setclosed);

        orders.setLayoutManager(new LinearLayoutManager(this));

        manager = getFragmentManager();

        noOrderWarn = (TextView)findViewById(R.id.branch_manager_activity_noorderswarn);

        /** Logic */

        setOpened.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatus(true);

            }
        });

        setClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatus(false);
            }
        });

        getOrders();
    }

    private void setStatus(final Boolean status) {
        mBranch.setIsOpen(status);
        mBranch.getLocation().addMetadata("isOpen",status);
        mBranch.getLocation().addMetadata("branchId",mBranch.getObjectId());
        mBranch.getLocation().addMetadata("imageUrl",mBranch.getImageUrl());
        Backendless.UserService.CurrentUser().setProperty("workingBranch",mBranch);
        BackendlessDataQuery query = new BackendlessDataQuery("branches.objectId = '" + mBranch.getObjectId() + "'");
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if(response.getData().size() > 0)
                {
                    mBranch.getLocation().addMetadata("name",response.getData().get(0).getName());
                    mBranch.saveAsync(new AsyncCallback<Branch>() {
                        @Override
                        public void handleResponse(Branch response) {
                            Toast.makeText(BranchManagerActivity.this, "Branch updated", Toast.LENGTH_SHORT).show();
                            if(status)
                            {
                                setOpened.setEnabled(false);
                                setClosed.setEnabled(true);
                            }
                            else if(!status)
                            {
                                setOpened.setEnabled(true);
                                setClosed.setEnabled(false);
                            }
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    public void getOrders() {
        Backendless.Persistence.mapTableToClass("Branch",Branch.class);
        Backendless.Persistence.of(Branch.class).findById(branch.get("objectId").toString(), 2, new AsyncCallback<Branch>() {
            @Override
            public void handleResponse(Branch response) {
                mBranch = response;
                Log.d(TAG, "handleResponse: Worked");
                menu.showMenu(true);
                menu.showMenuButton(true);
                if(!mBranch.getIsOpen())
                {
                    setClosed.setEnabled(false);
                    setOpened.setEnabled(true);
                }
                else if(mBranch.getIsOpen())
                {
                    setClosed.setEnabled(true);
                    setOpened.setEnabled(false);
                }

                if (response.getOrders().size() > 0) {
                    ordersB = (ArrayList<Order>) response.getOrders();
                    Collections.sort(ordersB, new Comparator<Order>() {
                        @Override
                        public int compare(Order lhs, Order rhs) {
                            return lhs.getCreated().compareTo(rhs.getCreated());
                        }
                    });
                    Collections.reverse(ordersB);
                    adapter = new BranchOrderWorkerAdapter(BranchManagerActivity.this, ordersB,manager,false);
                    orders.setAdapter(adapter);
                    orders.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            if(dy > 20 && menu.getVisibility() == View.VISIBLE)
                            {
                                menu.hideMenu(true);
                            }
                            else if(dy < -20 && menu.getVisibility() == View.INVISIBLE)
                            {
                                menu.showMenu(true);
                            }
                        }
                    });
                } else {
                    noOrderWarn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrders();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders_filter,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            //TODO: add filter
            case R.id.orders_filter_menu_all:
                getOrders();
                noOrderWarn.setVisibility(View.INVISIBLE);
                break;
            case R.id.orders_filter_menu_pending:
                filterList("pending");
                break;
            case R.id.orders_filter_menu_accepted:
                filterList("accepted");
                break;
            case R.id.orders_filter_menu_done:
                filterList("done");
                break;
            case R.id.orders_filter_menu_canceled:
                filterList("denied");
                break;
        }
        return true;
    }

    private void filterList(String status){
        ArrayList<Order> filter = new ArrayList<>();

        if (ordersB != null) {
            for (Order order23 :
                    ordersB) {
                if(order23.getStatus().equals(status))
                {
                    filter.add(order23);
                }
            }
        }
        if(filter.size() != 0)
        {
            adapter.setOrders(filter);
        }
        else{
            Toast.makeText(BranchManagerActivity.this, "Empty", Toast.LENGTH_SHORT).show();
        }
    }

}


/**
 * The graveyard
 * */



//        Log.d(TAG, "getOrders: Worked");
//        BackendlessGeoQuery geoQuery = new BackendlessGeoQuery();
//        geoQuery.addCategory("branches");
////        HashMap metaSearch = new HashMap();
////        metaSearch.put("objectId",branch.get("objectId").toString());
////        geoQuery.setMetadata(metaSearch);
//        geoQuery.setIncludeMeta(true);
//        Log.d(TAG, "getOrders: IM IN ORDERS GETTING LOCATION");
//        Backendless.Geo.getPoints(geoQuery, new AsyncCallback<BackendlessCollection<GeoPoint>>() {
//            @Override
//            public void handleResponse(BackendlessCollection<GeoPoint> response) {
//                if(response.getData().size() == 1)
//                {
//                    for (GeoPoint tt:
//                            response.getData()) {
//                        if(point.getMetadata("branchId").equals(branch.get("objectId")))
//                        {
//                            point = tt;
//                        }
//                    }
//                    point = response.getData().get(0);
//                }
//            }
//
//            @Override
//            public void handleFault(BackendlessFault fault) {
//                Log.e(TAG, "handleFault: " + fault.getMessage());
//            }
//        });

//
//Backendless.Persistence.of(Branch.class).findById(branch.get("objectId").toString(), new AsyncCallback<Branch>() {
//@Override
//public void handleResponse(Branch response) {
//        mBranch = response;
//        menu.showMenu(true);
//        menu.showMenuButton(true);
//
//        response.setLocation(point);
//
//        response.saveAsync(new AsyncCallback<Branch>() {
//@Override
//public void handleResponse(Branch response) {
//        Log.d(TAG, "handleResponse: branch saved");
//        }
//
//@Override
//public void handleFault(BackendlessFault fault) {
//        Log.e(TAG, "handleFault: " + fault.getMessage());
//        }
//        });
//
//        if(!mBranch.getIsOpen())
//        {
//        setClosed.setEnabled(false);
//        setOpened.setEnabled(true);
//        } else if(mBranch.getIsOpen())
//        {
//        setClosed.setEnabled(true);
//        setOpened.setEnabled(false);
//        }
//
//        adapter = new BranchOrderWorkerAdapter(BranchManagerActivity.this, (ArrayList<Order>) response.getOrders(),manager);
//        orders.setAdapter(adapter);
//        }
//
//@Override
//public void handleFault(BackendlessFault fault) {
//        Log.e(TAG, "handleFault: " + fault.getMessage());
//        }
//        });