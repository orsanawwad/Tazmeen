package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.github.clans.fab.FloatingActionButton;

import net.crofis.tazmeen.Adapters.OwnerViewFoodAdapter;
import net.crofis.tazmeen.Adapters.OwnerViewFoodExtraAdapter;
import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OwnerViewFoodExtrasActivity extends AppCompatActivity {

    private static final String TAG = "OViewFood";

    FloatingActionButton addExtra;

    RecyclerView foodExtraRecyclerView;

    OwnerViewFoodExtraAdapter adapter;

    TextView noFoodExtraWarn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_view_food_extras);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Food extras");

        addExtra = (FloatingActionButton)findViewById(R.id.owner_view_foodextra_activity_addfoodextra);

        foodExtraRecyclerView = (RecyclerView)findViewById(R.id.owner_view_foodextra_activity_foodextralist);

        foodExtraRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        noFoodExtraWarn = (TextView)findViewById(R.id.owner_view_foodextra_activity_nofoodextra);

        addExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(OwnerViewFoodExtrasActivity.this,OwnerAddFoodExtraActivity.class),1111);
            }
        });

        fetchFoodExtra();
    }

    private void fetchFoodExtra() {
        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if (response.getData().size() == 1)
                {
                    ArrayList<FoodExtra> extras = (ArrayList<FoodExtra>) response.getData().get(0).getFoodextras();
                    if (extras.size() > 0) {
//                        Collections.reverse(extras);
                        Collections.sort(extras, new Comparator<FoodExtra>() {
                            @Override
                            public int compare(FoodExtra lhs, FoodExtra rhs) {
                                return lhs.getCreated().compareTo(rhs.getCreated());
                            }
                        });
                        Collections.reverse(extras);
                        adapter = new OwnerViewFoodExtraAdapter(OwnerViewFoodExtrasActivity.this, extras);
                        foodExtraRecyclerView.setAdapter(adapter);
                        foodExtraRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if(dy > 20 && addExtra.getVisibility() == View.VISIBLE)
                                {
                                    addExtra.hide(true);
                                }
                                else if(dy < -20 && addExtra.getVisibility() == View.INVISIBLE)
                                {
                                    addExtra.show(true);
                                }
                            }
                        });
                    } else {
                        noFoodExtraWarn.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK)
        {
            Log.d(TAG, "RECEIVED");
            fetchFoodExtra();
            noFoodExtraWarn.setVisibility(View.INVISIBLE);
        }
    }
}
