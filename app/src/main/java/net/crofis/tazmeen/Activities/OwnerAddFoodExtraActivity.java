package net.crofis.tazmeen.Activities;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.ui.dialog.LoadingDialog;

public class OwnerAddFoodExtraActivity extends AppCompatActivity {

    private static final String TAG = "OwnerFoodExtraAdd";

    /** EditTexts for name and price */
    TextInputEditText name,price;

    /** Button for adding extra */
    Button addExtra;

    /** Food Extra object to store on backendless */
    FoodExtra foodExtra;

    LoadingDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_add_food_extra);
        Log.d(TAG, "onCreate: Activity started");

        foodExtra = new FoodExtra();
        Log.d(TAG, "onCreate: FoodExtra Object created");

        // Initialize the UI
        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("New food extra");

        /**
         * Find views
         */
        name = (TextInputEditText)findViewById(R.id.owner_add_food_extra_name);
        price = (TextInputEditText)findViewById(R.id.owner_add_food_extra_price);
        addExtra = (Button)findViewById(R.id.owner_add_food_extra_addExtra);

        /**
         * Set listeners
         */

        /** When focus is lost save name */
        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    foodExtra.setName(name.getText().toString());
                }
            }
        });

        /** When focus is lost save price */
        price.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    foodExtra.setPrice(Double.parseDouble(price.getText().toString()));
                }
            }
        });

        /** Submit FoodExtra to BackendLess */
        addExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addExtra();
            }
        });
    }

    /** Just check for name and save it, price is now optional */
    private void addExtra() {
        if(name.getText().toString().trim().isEmpty())
        {
            Toast.makeText(OwnerAddFoodExtraActivity.this, "Please recheck name", Toast.LENGTH_SHORT).show();
        }
        else
        {
            foodExtra.setName(name.getText().toString().trim());
            if (!price.getText().toString().isEmpty()) {
                if(Double.parseDouble(price.getText().toString().trim()) > 0)
                    foodExtra.setPrice(Double.parseDouble(price.getText().toString().trim()));
                else
                    Toast.makeText(OwnerAddFoodExtraActivity.this, "If you want to add price please make it above 0", Toast.LENGTH_SHORT).show();
            }
            else {
                dialog = new LoadingDialog(this,"Adding food extra...");
                dialog.getMessage().setText("Fetching restaurant...");
                dialog.show();
                String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
                BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
                Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
                    @Override
                    public void handleResponse(BackendlessCollection<Restaurant> response) {
                        if(response.getData().size() == 1)
                        {
                            dialog.getMessage().setText("Updating restaurant...");
                            Restaurant restaurant = response.getData().get(0);
                            restaurant.getFoodextras().add(foodExtra);
                            restaurant.saveAsync(new AsyncCallback<Restaurant>() {
                                @Override
                                public void handleResponse(Restaurant response) {
                                    Toast.makeText(OwnerAddFoodExtraActivity.this, "Food extra added.", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                    setResult(RESULT_OK);
                                    finish();
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    Log.e(TAG, "handleFault: " + fault.getMessage());
                                    dialog.dismiss();
                                }
                            });
                        }
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.e(TAG, "handleFault: " + fault.getMessage());
                        dialog.dismiss();
                    }
                });
            }
        }
    }
}


/**
 * The graveyard
 */

//        if(foodExtra.getName() == null || foodExtra.getName().isEmpty())
//        {
//            if(!name.getText().toString().trim().isEmpty())
//            {
//                foodExtra.setName(name.getText().toString());
//            }
//            else
//            {
//                Toast.makeText(OwnerAddFoodExtraActivity.this, "Please enter something", Toast.LENGTH_SHORT).show();
//            }
//        }
//        if(foodExtra.getPrice() == null)
//        {
//            if(!price.getText().toString().trim().isEmpty())
//            {
//                foodExtra.setPrice(Double.parseDouble(price.getText().toString()));
//            }
//            else
//            {
//                Toast.makeText(OwnerAddFoodExtraActivity.this, "Please enter something", Toast.LENGTH_SHORT).show();
//            }
//        }
//        if(foodExtra.getName().isEmpty())
//        {
//            Toast.makeText(OwnerAddFoodExtraActivity.this, "Please enter a name and a price higher than 0.0", Toast.LENGTH_SHORT).show();
//        }
//        else
//        {
//            String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
//            BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
//            Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
//                @Override
//                public void handleResponse(BackendlessCollection<Restaurant> response) {
//                    if(response.getData().size() == 1)
//                    {
//                        Restaurant restaurant = response.getData().get(0);
//                        restaurant.getFoodextras().add(foodExtra);
//                        restaurant.saveAsync(new AsyncCallback<Restaurant>() {
//                            @Override
//                            public void handleResponse(Restaurant response) {
//                                Toast.makeText(OwnerAddFoodExtraActivity.this, "Food extra added.", Toast.LENGTH_SHORT).show();
//                                setResult(RESULT_OK);
//                                finish();
//                            }
//
//                            @Override
//                            public void handleFault(BackendlessFault fault) {
//                                Log.e(TAG, "handleFault: " + fault.getMessage());
//                            }
//                        });
//                    }
//                }
//
//                @Override
//                public void handleFault(BackendlessFault fault) {
//                    Log.e(TAG, "handleFault: " + fault.getMessage());
//                }
//            });
//        }
