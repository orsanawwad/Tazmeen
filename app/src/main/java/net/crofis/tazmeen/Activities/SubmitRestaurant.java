package net.crofis.tazmeen.Activities;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.LoadingDialog;

public class SubmitRestaurant extends AppCompatActivity {

    private static final String TAG = "SubmitRestaurant";

    TextInputEditText name;

    Button publish;

    BackendlessUser user;

    LoadingDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_restaurant);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("New restaurant");

        name = (TextInputEditText)findViewById(R.id.submit_restaurant_activity_name);

        user = Backendless.UserService.CurrentUser();

        publish = (Button)findViewById(R.id.submit_restaurant_activity_publish);

        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!name.getText().toString().trim().isEmpty())
                {
                    dialog = new LoadingDialog(SubmitRestaurant.this,"Adding restaurant...");

                    dialog.show();
                    Restaurant restaurant = new Restaurant();
                    restaurant.setName(name.getText().toString().trim());
                    restaurant.setLogo("RANDOM");
                    dialog.getMessage().setText("Saving restaurant...");
                    restaurant.saveAsync(new AsyncCallback<Restaurant>() {
                        @Override
                        public void handleResponse(Restaurant response) {
                            user.setProperty("userRole", Starter.Constants.USER_ROLE_OWNER);
                            dialog.getMessage().setText("Success, updating user...");
                            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                                @Override
                                public void handleResponse(BackendlessUser response) {
                                    setResult(RESULT_OK);
                                    dialog.dismiss();
                                    Toast.makeText(SubmitRestaurant.this, "Success, please logout and log back in for changes to take effect.", Toast.LENGTH_SHORT).show();
                                    finish();
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    Log.e(TAG, "handleFault: " + fault.getMessage());
                                    dialog.dismiss();
                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                            dialog.dismiss();
                        }
                    });
                }
            }
        });
    }
}


