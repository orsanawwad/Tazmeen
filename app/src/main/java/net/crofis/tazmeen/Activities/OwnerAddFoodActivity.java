package net.crofis.tazmeen.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.backendless.persistence.BackendlessDataQuery;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.desmond.squarecamera.CameraActivity;
import com.github.clans.fab.FloatingActionButton;
import com.nineoldandroids.animation.Animator;
import com.soundcloud.android.crop.Crop;

import net.crofis.tazmeen.Adapters.OwnerAddFoodExtraAdapter;
import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.DialogManager;
import net.crofis.ui.dialog.InfoDialog;
import net.crofis.ui.dialog.LoadingDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class OwnerAddFoodActivity extends AppCompatActivity {

    private static final String TAG = "OwnerAddFoodActivity";

    /** Name EditText input for food name */
    TextInputEditText name;

    /** Price EditText input for food price */
    TextInputEditText price;

    /** Add Extras button to add extras to food */
    Button addExtras;

    /** Add Food button to add food to server */
    Button addFood;

    /** Food image to preview the image taken for food */
    ImageView foodImage;

    /** Food object to store data */
    Food food;

    /** Food extras array list to define the extras the food has */
    ArrayList<FoodExtra> foodExtras;

    /** Image uri to save image from camera or pick from gallery */
    Uri imageUri;

    /** Recycler view used to add the food extras for the user */
    RecyclerView foodExtrasRecyclerView;

    /** Relative layout used just to dismiss recyclerview */
    RelativeLayout recyclerViewRelativeLayout;

    /** Recycler view adapter for supplying data recycler view list */
    OwnerAddFoodExtraAdapter adapter;

    /** FloatingActionButton to open Add Extra to database activity */
    FloatingActionButton startAddExtraActivity;

    /** Pick image TextView*/
    TextView pickImage;

    /** Uri used for cropping */
    Uri destination;

    /** Dialog for notifying the stages */
    LoadingDialog dialog;

    String regex = "\\d+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_add_food);
        Log.d(TAG, "onCreate: Owner Add Food Activity started");

        food = new Food();
        Log.d(TAG, "onCreate: Food Created");

        foodExtras = new ArrayList<>();
        Log.d(TAG, "onCreate: Food extra array created");

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("New food");

        /**
         * Find the views
         * */
        name = (TextInputEditText)findViewById(R.id.owner_add_food_activity_name);

        price = (TextInputEditText)findViewById(R.id.owner_add_food_activity_price);

        addExtras = (Button)findViewById(R.id.owner_add_food_activity_addextras);

        addFood = (Button)findViewById(R.id.owner_add_food_activity_addfood);

        foodImage = (ImageView)findViewById(R.id.owner_add_food_activity_foodImage);

        foodExtrasRecyclerView = (RecyclerView)findViewById(R.id.owner_add_food_activity_foodextraslist_recyclerview);

        recyclerViewRelativeLayout = (RelativeLayout)findViewById(R.id.owner_add_food_activity_layoutlayerforrecyclerview);

        startAddExtraActivity = (FloatingActionButton)findViewById(R.id.owner_add_food_activity_fab_addextra);

        pickImage = (TextView)findViewById(R.id.owner_add_food_activity_pickimage);

        /** Add layout manager */

        foodExtrasRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        populateFoodExtra();

        foodExtrasRecyclerView.setAdapter(adapter);

        /**
         * Add Listeners
         * */

        /** Click layout aka anywhere to dismiss RecyclerView */
        recyclerViewRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExtras();
            }
        });


        /** Click image to start picking image from gallery or camera */
        foodImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Add image functionality
                startImageTakingOrPicking();
            }
        });

        /** On click check for info and add food to BackendLess */
        addFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFood();
            }
        });

        /** On focus lose save name to food object */
        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    food.setName(name.getText().toString());
                }
            }
        });

        //TODO: Add watcher to default only to 0.01$ minimum
        /** On focus lose save price to food object */
        price.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String priceText = price.getText().toString();
                    if(priceText.matches(regex))
                    {
                        if(Double.parseDouble(price.getText().toString()) != 0)
                        {
                            food.setPrice(Double.parseDouble(price.getText().toString()));
                        }
                        else
                        {
//                          price.setText("0.1");
//                          food.setPrice(0.1);
                            Toast.makeText(OwnerAddFoodActivity.this, "Food must be above 0 dollar", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(OwnerAddFoodActivity.this, "Only digits", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        /** Start Activity */
        startAddExtraActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddExtraActivity();
            }
        });

    }

    /** Populate food extras, first the color will be grey, then it'll become blue after things are loaded */
    private void populateFoodExtra() {
        addExtras.setBackgroundColor(Color.parseColor("#9E9E9E"));
        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() +"'";
        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if(response.getData().size() == 1)
                {
                    if(response.getData().get(0).getFoodextras().size() > 0)
                    {
                        adapter = new OwnerAddFoodExtraAdapter((ArrayList<FoodExtra>) response.getData().get(0).getFoodextras(),getApplicationContext());
                        foodExtrasRecyclerView.setAdapter(adapter);
                        addExtras.setBackgroundColor(Color.parseColor("#1976d2"));
                        addExtras.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showExtras();
                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(OwnerAddFoodActivity.this, "You don't have any Food Extras Available, click pick extra to add one", Toast.LENGTH_LONG).show();
                        addExtras.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addExtras.setBackgroundColor(Color.parseColor("#1976d2"));
                                startAddExtraActivity();
                            }
                        });
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault);
            }
        });
    }

    /** Start the process to either take image or pick image, from dialog (change that to make it more pro) */
    private void startImageTakingOrPicking() {
        final CharSequence[] items = { "Take Photo from Camera", "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo from Camera")) {
                    Intent intent = new Intent(OwnerAddFoodActivity.this, CameraActivity.class);
                    startActivityForResult(intent,Starter.Constants.ADD_FOOD_REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            Starter.Constants.ADD_FOOD_REQUEST_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    /** This starts the add extra activity */
    private void startAddExtraActivity(){
        Intent intent = new Intent(OwnerAddFoodActivity.this,OwnerAddFoodExtraActivity.class);
        startActivityForResult(intent, Starter.Constants.ADD_EXTRA_REQUEST);
    }

    /** Show extra's RecyclerView with animations */
    private void showExtras() {
        if(adapter != null)
        {
            if(foodExtrasRecyclerView.getVisibility() == View.INVISIBLE)
            {
                recyclerViewRelativeLayout.setVisibility(View.VISIBLE);
                foodExtrasRecyclerView.setVisibility(View.VISIBLE);
                startAddExtraActivity.show(true);
                YoYo.with(Techniques.SlideInUp).duration(230).playOn(foodExtrasRecyclerView);
            }
            else
            {
                recyclerViewRelativeLayout.setVisibility(View.INVISIBLE);
                startAddExtraActivity.hide(true);
                YoYo.with(Techniques.SlideOutDown).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        foodExtrasRecyclerView.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).duration(300).playOn(foodExtrasRecyclerView);
            }
        }
    }


    /** Check for input like name etc and add it to BackendLess */
    private void addFood() {
        /**
         *
         * Dialog for notifying the stages
         *
         * */

        dialog = new LoadingDialog(this,"Saving item...");

        dialog.show();

        dialog.getMessage().setText("Checking info...");

        addFood.setEnabled(false);

        /**
         * 1. check for inputs
         * 2. save object food
         * 3. upload image
         * 4. get url and update it to food object
         * 5. get owner's restaurant object and save the food
         * 6. finish activity with OK result
         */
        if(food.getName() == null)
        {
            if(name.getText().toString().isEmpty())
            {
                Toast.makeText(OwnerAddFoodActivity.this, "Enter name", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                addFood.setEnabled(true);
            }
            else food.setName(name.getText().toString());
        }
        if (food.getPrice() == null)
        {
            if(price.getText().toString().isEmpty())
            {
                Toast.makeText(OwnerAddFoodActivity.this, "Enter price", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                addFood.setEnabled(true);
            }
            else {
                String priceText = price.getText().toString();
                if(priceText.matches(regex)) {
                    if (Double.parseDouble(price.getText().toString()) != 0.0) {
                        food.setPrice(Double.parseDouble(price.getText().toString()));
                    } else {
                        //                    price.setText("0.1");
                        //                    food.setPrice(0.1);
                        Toast.makeText(OwnerAddFoodActivity.this, "Price must be above 0 dollar", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        addFood.setEnabled(true);
                    }
                } else {
                    Toast.makeText(OwnerAddFoodActivity.this, "Digits only", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    addFood.setEnabled(true);
                }
            }
        }
        if (imageUri == null)
        {
            Toast.makeText(OwnerAddFoodActivity.this, "Please add image", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            addFood.setEnabled(true);
        }
        if(food.getName() != null && food.getPrice() != null)
        {
            try {
                food.setFoodextras(adapter.getSelected());
            } catch (Exception e) {

            }
        }
//        && imageUri != null
        if(food.getName() == null || food.getPrice() == null || imageUri == null || name.getText().toString().equals("")) {
            Toast.makeText(OwnerAddFoodActivity.this, "Recheck what you entered...", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            addFood.setEnabled(true);
        }
        else if(food.getPrice() == 0.0 || price.getText().toString().equals("") || Double.parseDouble(price.getText().toString()) == 0.0 ||
                !price.getText().toString().matches(regex))
        {
            Toast.makeText(OwnerAddFoodActivity.this, "Price must be above 0", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            addFood.setEnabled(true);
        }
        else if(!food.getName().isEmpty() && !food.getPrice().isNaN())
//        else
        {
            dialog.getMessage().setText("Populating data...");
            Log.d(TAG, "addFood: Name: " + food.getName());
            Log.d(TAG, "addFood: Price: " + food.getPrice());
            try {
                Log.d(TAG, "addFood: ArrayList count: " + food.getFoodextras().size());
            } catch (Exception e) {

            }
            Log.d(TAG, "addFood: hasImage: " + true);
            dialog.getMessage().setText("Uploading item..");
            food.saveAsync(new AsyncCallback<Food>() {
                @Override
                public void handleResponse(final Food food1) {
                    Bitmap bitmap = null;
//                    try {
//                        bitmap = MediaStore.Images.Media.getBitmap(OwnerAddFoodActivity.this.getContentResolver(), imageUri);
                    bitmap = rotateIfNeccessary(imageUri);

                    bitmap = Bitmap.createScaledBitmap(bitmap,512,512,false);
// bitmap = rotateIfNeccessary(imageUri);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    Log.d(TAG, "handleResponse: Bitmap created");
                    dialog.getMessage().setText("Uploading photo...");
                    Backendless.Files.Android.upload(bitmap, Bitmap.CompressFormat.PNG, 100, food1.getObjectId() + ".jpg", "Foods", new AsyncCallback<BackendlessFile>() {
                        @Override
                        public void handleResponse(BackendlessFile file) {
                            food1.setImageUrl(file.getFileURL());
                            dialog.getMessage().setText("Updating item...");
                            food1.saveAsync(new AsyncCallback<Food>() {
                                @Override
                                public void handleResponse(final Food food2) {
                                    Log.d(TAG, "handleResponse: Food image url updated");
                                    String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
                                    BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
                                    Log.d(TAG, "handleResponse: Restaurant fetched");
                                    Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
                                        @Override
                                        public void handleResponse(BackendlessCollection<Restaurant> response) {
                                            if(response.getData().size() == 1)
                                            {
                                                Restaurant restaurant = response.getData().get(0);
                                                restaurant.getFoods().add(food2);
                                                dialog.getMessage().setText("Updating restaurant...");
                                                restaurant.saveAsync(new AsyncCallback<Restaurant>() {
                                                    @Override
                                                    public void handleResponse(Restaurant response) {
                                                        dialog.dismiss();
                                                        Toast.makeText(OwnerAddFoodActivity.this, "Food added", Toast.LENGTH_SHORT).show();
                                                        setResult(RESULT_OK);
                                                        finish();
                                                    }

                                                    @Override
                                                    public void handleFault(BackendlessFault fault) {
                                                        Log.e(TAG, "handleFault: " + fault.toString());
                                                        dialog.dismiss();
                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void handleFault(BackendlessFault fault) {
                                            Log.e(TAG, "handleFault: " + fault.toString());
                                            dialog.dismiss();
                                        }
                                    });
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    Log.e(TAG, "handleFault: " + fault.toString());
                                    dialog.dismiss();
                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.toString());
                            dialog.dismiss();
                        }
                    });
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "handleFault: " + fault.toString());
                    dialog.dismiss();
                }
            });
        }
        else {
            Toast.makeText(OwnerAddFoodActivity.this, "Something went wront...", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            addFood.setEnabled(true);
        }
    }


    /** this checks for photos, cropped ones too, AND check if user added new FoodExtra, if so repopulate it */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == Crop.REQUEST_CROP){
            imageUri = Crop.getOutput(data);

//            rotateIfNeccessary(imageUri);

            foodImage.setVisibility(View.VISIBLE);

            Glide.with(this).load(imageUri).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(foodImage);
            pickImage.setVisibility(View.GONE);
        }
        if(resultCode != RESULT_OK);
        if(resultCode == RESULT_OK && requestCode == Starter.Constants.ADD_EXTRA_REQUEST)
        {
            populateFoodExtra();
        }
        else if (resultCode == RESULT_OK && requestCode == Starter.Constants.ADD_FOOD_REQUEST_CAMERA)
        {
            imageUri = data.getData();
            foodImage.setVisibility(View.VISIBLE);
            Glide.with(this).load(imageUri).into(foodImage);
            pickImage.setVisibility(View.GONE);
        }
        else if (resultCode == RESULT_OK && requestCode == Starter.Constants.ADD_FOOD_REQUEST_FILE) {
            Uri selectedImageUri = data.getData();
            destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
            Crop.of(selectedImageUri, destination).asSquare().start(this);
        }
    }

    private Bitmap rotateIfNeccessary(Uri imageUri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            ExifInterface exif = new ExifInterface(imageUri.getPath());
            int rotation = exif.getAttributeInt(    ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {matrix.preRotate(rotationInDegrees);}
            Bitmap bitmap1 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            return bitmap1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    /** This is just used to dismiss FoodExtra list picking if it was visible, else go back, For the sake of UX */
    @Override
    public void onBackPressed() {
        if(recyclerViewRelativeLayout.getVisibility() == View.VISIBLE)
        {
            showExtras();
        }
        else super.onBackPressed();
    }
}


/**
 *  The graveyard
 */


//        /** Pick the food extras for the food object */
//        addExtras.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //TODO: Promote user to choose extras using dialog
//                startExtraAdding();
//            }
//        });