package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.crofis.tazmeen.Adapters.LoginRegisterAdapter;
import net.crofis.tazmeen.R;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;

public class LoginActivity extends AppCompatActivity {

    VerticalViewPager loginRegisterViewPager;
    LoginRegisterAdapter loginRegisterAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginRegisterViewPager = (VerticalViewPager) findViewById(R.id.login_activity);
        loginRegisterAdapter = new LoginRegisterAdapter(getSupportFragmentManager());
        loginRegisterViewPager.setAdapter(loginRegisterAdapter);
    }

    public void endActivity(){
        setResult(RESULT_OK);
        finish();
    }
}
