package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.github.clans.fab.FloatingActionButton;

import net.crofis.tazmeen.Adapters.OwnerViewBranchAdapter;
import net.crofis.tazmeen.Adapters.OwnerViewFoodAdapter;
import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OwnerViewBranchesActivity extends AppCompatActivity {

    private static final String TAG = "OViewBranches";

    /** Start add branch activity */
    FloatingActionButton addBranch;

    /** View food list */
    RecyclerView branchRecyclerView;

    /** Adapter fod branches */
    OwnerViewBranchAdapter adapter;

    TextView noBranchesWarn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_view_branches);

        //TODO: Add option to edit and remove

        /** Start ui */
        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Branches");

        addBranch = (FloatingActionButton)findViewById(R.id.owner_view_branches_activity_addbranch);

        branchRecyclerView = (RecyclerView)findViewById(R.id.owner_view_branches_activity_branchlist);

        branchRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        noBranchesWarn = (TextView)findViewById(R.id.owner_view_branches_activity_nobrancheswarn);

        addBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(OwnerViewBranchesActivity.this,OwnerAddBranchActivity.class),9852);
            }
        });

        fetchBranches();
    }

    private void fetchBranches() {
        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if (response.getData().size() == 1)
                {
                    ArrayList<Branch> branches = (ArrayList<Branch>) response.getData().get(0).getBranches();
                    if (branches.size() > 0) {
//                        Collections.reverse(branches);
                        Collections.sort(branches, new Comparator<Branch>() {
                            @Override
                            public int compare(Branch lhs, Branch rhs) {
                                return lhs.getCreated().compareTo(rhs.getCreated());
                            }
                        });
                        Collections.reverse(branches);
                        adapter = new OwnerViewBranchAdapter(branches,OwnerViewBranchesActivity.this);
                        branchRecyclerView.setAdapter(adapter);
                        branchRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if(dy > 20 && addBranch.getVisibility() == View.VISIBLE)
                                {
                                    addBranch.hide(true);
                                }
                                else if(dy < -20 && addBranch.getVisibility() == View.INVISIBLE)
                                {
                                    addBranch.show(true);
                                }
                            }
                        });
                    } else {
                        noBranchesWarn.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fetchBranches();
        noBranchesWarn.setVisibility(View.INVISIBLE);
    }
}
