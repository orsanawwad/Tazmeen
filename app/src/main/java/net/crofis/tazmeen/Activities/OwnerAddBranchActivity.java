package net.crofis.tazmeen.Activities;

import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.desmond.squarecamera.CameraActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.soundcloud.android.crop.Crop;

import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.LoadingDialog;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class OwnerAddBranchActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "OwnerAddBranchActivity";

    /** Buttons to add GeoPoint and submit branch to server */
    Button addPoint,addBranch;

    /** Address text input for specifying the address of the branch, and username is to set the specified user as a worker for the branch */
    TextInputEditText address,username;

    /** The image of branch, clicking it will give you to choose an image or take from camera, MUST BE SQUARED */
    ImageView branchImage;

    /** Map parent layout to show the map after user clicks add point button */
    RelativeLayout mapParentLayout;

    /** Map fragment object to change mode to lite */
    MapFragment mapFragment;

    /** Map object to display point in lite mode */
    GoogleMap mMap;

    /** The branch subject that is used to store the properties in order to save it to the server */
    Branch branch;

    /** Branch's restaurant parent so we can use it to save the branch to the server with relation to parent restaurant */
    Restaurant restaurant;

    /** User that will become worker in the branch, get the user once owner typed his username */
    BackendlessUser user;

    /** Branch image uri to use for saving branch image */
    Uri imageUri;

    /** Textview to let the user pick an image */
    TextView pickImage;

    /** Uri destination of cropped photo */
    Uri destination;

    /** Alert dialog */
    LoadingDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_add_branch);

        //TODO: Add check flag to check if user wants to edit a branch
        branch = new Branch();

        //Init the UI
        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("New branch");

        /** Find views */
        addPoint = (Button)findViewById(R.id.owner_add_branch_activity_addpoint);
        addBranch = (Button)findViewById(R.id.owner_add_branch_activity_add);
        address = (TextInputEditText)findViewById(R.id.owner_add_branch_activity_address);
        username = (TextInputEditText)findViewById(R.id.owner_add_branch_activity_username);
        branchImage = (ImageView)findViewById(R.id.owner_add_branch_activity_imageview);
        mapParentLayout = (RelativeLayout)findViewById(R.id.owner_add_branch_activity_maplayout);
        pickImage = (TextView)findViewById(R.id.owner_add_branch_activity_pickimage);

        /** Add listeners */

        /** Click listeners */

        /** addPoint on click start location picker */
        addPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationPicker();
            }
        });

        /** addBranch on click check for inputs then */
        addBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBranch();
            }
        });

        /** branchImage on click listener choose photo or take photo with camera (SQUARE PHOTO IS MUST) */
        branchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startImageTakingOrPicking();
            }
        });

        /** username focus listener, wait for username element to lose focus to send request to backendless and check if user exists */
        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    checkUsername(username.getText().toString().trim());
                }
            }
        });

    }


    /** Add branch logic */
    private void addBranch() {

        dialog = new LoadingDialog(this,"Saving branch...");

        dialog.getMessage().setText("Checking info...");

        dialog.show();

        //Check if user has already been pulled, if not run checking again with save parameter
        if(user == null)
        {
            checkUsername(username.getText().toString().trim(),true);
        }
        else{
            //Check if branch location already set and branch text too, if not notify user
            if(branch.getLocation() == null || branch.getAddress() == null)
            {
                address.setError("No location specified");
                Toast.makeText(OwnerAddBranchActivity.this, "You haven't picked a location point for the branch", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
            if (!address.getText().toString().equals(""))
            {
                branch.setAddress(address.getText().toString());
            }
            else{
                Toast.makeText(OwnerAddBranchActivity.this, "Please enter address", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
            //Check if image already taken, if not notify user
            if (imageUri == null)
            {
                Toast.makeText(OwnerAddBranchActivity.this, "You haven't picked an image, please pick one", Toast.LENGTH_SHORT).show();
            }
            else {
                dialog.getMessage().setText("Uploading item...");
                //Set branch as not opened
                branch.setIsOpen(false);
                //Log current data for debug
                Log.d(TAG, "addBranch: " + branch.getAddress());
                Log.d(TAG, "addBranch: " + branch.getIsOpen());
                Log.d(TAG, "addBranch: " + user.getEmail());
                /**
                 * The steps taken to save a complete branch:
                 * 1. Save current branch
                 * 2. Upload image with branch objectId as its name
                 * 3. Take the image url and set the branch imageUrl to image, get GeoPoint, set Category to branches, add metadata objectId to the branch's object id and save branch
                 * 4. If everything went well get the owner restaurant object, add branch, save it
                 * 5. Get the user that the owner chose to work, change role to worker, add branch as his working place
                 * 6. Finish activity
                 * */
                //1
                final GeoPoint temp = branch.getLocation();
                branch.setLocation(null);
                branch.saveAsync(new AsyncCallback<Branch>() {
                    @Override
                    public void handleResponse(final Branch rBranch) {
                        //2
                        Log.d(TAG, "handleResponse: Branch saved");
                        Bitmap bitmap = null;
//                        try {
//                            bitmap = MediaStore.Images.Media.getBitmap(OwnerAddBranchActivity.this.getContentResolver(), imageUri);
                        bitmap = rotateIfNeccessary(imageUri);

                        bitmap = Bitmap.createScaledBitmap(bitmap,512,512,false);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                        Log.d(TAG, "handleResponse: Bitmap created");
                        dialog.getMessage().setText("Uploading photo...");
                        Backendless.Files.Android.upload(bitmap, Bitmap.CompressFormat.PNG, 100, rBranch.getObjectId() + ".jpg", "Branches", new AsyncCallback<BackendlessFile>() {
                            @Override
                            public void handleResponse(BackendlessFile response) {
                                //3
                                Log.d(TAG, "handleResponse: Picture uploaded");
                                rBranch.setImageUrl(response.getFileURL());
                                rBranch.setLocation(temp);
                                rBranch.getLocation().addCategory("branches");
                                rBranch.getLocation().addMetadata("branchId",rBranch.getObjectId());
                                Log.d(TAG, "handleResponse: Branch image url, location category and metadata set");
                                dialog.getMessage().setText("Updating data...");
                                rBranch.saveAsync(new AsyncCallback<Branch>() {
                                    @Override
                                    public void handleResponse(final Branch rBranch2) {
                                        //4
                                        Log.d(TAG, "handleResponse: Branch image url, location metadata and category updated");
                                        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
                                        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
                                        Log.d(TAG, "handleResponse: Restaurant fetched");
                                        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
                                            @Override
                                            public void handleResponse(BackendlessCollection<Restaurant> response) {
                                                if(response.getData().size() == 1)
                                                {
                                                    Log.d(TAG, "handleResponse: Restaurant fetched successfully");
                                                    final Restaurant restaurant = response.getData().get(0);
                                                    rBranch2.getLocation().addMetadata("imageUrl",rBranch2.getImageUrl());
                                                    rBranch2.getLocation().addMetadata("name",restaurant.getName());
                                                    rBranch2.getLocation().addMetadata("isOpen",false);
                                                    rBranch2.setRestaurantName(restaurant.getName());
                                                    dialog.getMessage().setText("Updating location...");
                                                    rBranch2.saveAsync(new AsyncCallback<Branch>() {
                                                        @Override
                                                        public void handleResponse(final Branch rBranch3) {
                                                            restaurant.getBranches().add(rBranch3);
                                                            dialog.getMessage().setText("Updating restaurant...");
                                                            restaurant.saveAsync(new AsyncCallback<Restaurant>() {
                                                                @Override
                                                                public void handleResponse(Restaurant response) {
                                                                    //5
                                                                    user.setProperty("userRole",Starter.Constants.USER_ROLE_WORKER);
                                                                    user.setProperty("workName",response.getName());
                                                                    user.setProperty("workId",rBranch3.getObjectId());
                                                                    user.setProperty("workBranch",rBranch3);
                                                                    dialog.getMessage().setText("Updating user/worker...");
                                                                    Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                                                                        @Override
                                                                        public void handleResponse(BackendlessUser response) {
                                                                            //6
                                                                            dialog.dismiss();
                                                                            finish();
                                                                            setResult(RESULT_OK);
                                                                            Toast.makeText(OwnerAddBranchActivity.this, "Success, branch added.", Toast.LENGTH_SHORT).show();
                                                                        }

                                                                        @Override
                                                                        public void handleFault(BackendlessFault fault) {
                                                                            Log.i(TAG, "6. handleFault: " + fault.getMessage());
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                                }

                                                                @Override
                                                                public void handleFault(BackendlessFault fault) {
                                                                    Log.i(TAG, "5. handleFault: " + fault.getMessage());
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                        }

                                                        @Override
                                                        public void handleFault(BackendlessFault fault) {
                                                            Log.e(TAG, "4.5. handleFault: " + fault.getMessage());
                                                            dialog.dismiss();
                                                        }
                                                    });
                                                }
                                                else{
                                                    Log.e(TAG, "handleResponse: Many or no restaurant/s were fetched, something is not right, check the database\nCurrent userId: " + Backendless.UserService.CurrentUser().getObjectId()) ;
                                                    dialog.dismiss();
                                                }
                                            }

                                            @Override
                                            public void handleFault(BackendlessFault fault) {
                                                Log.i(TAG, "4. handleFault: " + fault.getMessage());
                                                dialog.dismiss();
                                            }
                                        });
                                    }

                                    @Override
                                    public void handleFault(BackendlessFault fault) {
                                        Log.i(TAG, "3. handleFault: " + fault.getMessage());
                                        dialog.dismiss();
                                    }
                                });
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.i(TAG, "2. handleFault: " + fault.getMessage());
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.i(TAG, "1. handleFault: " + fault.getMessage());
                        dialog.dismiss();
                    }
                });
            }
        }
    }

    /** Check if username exists in BackendLess exists so we can update it later once owner, if shouldSave is true execute addBranch() */
    private void checkUsername(String tUsername, final boolean shouldSave) {
        String whereClause = "userName = '" + tUsername + "'";
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause( whereClause );
        Backendless.Persistence.of(BackendlessUser.class).find(dataQuery, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                if(response.getData().size() == 1)
                {
                    if(!response.getData().get(0).getObjectId().equals(Backendless.UserService.CurrentUser().getObjectId()) || Integer.getInteger(response.getData().get(0).getProperty("userRole").toString()) != 0){
                        user = response.getData().get(0);
                        if(shouldSave)
                        {
                            addBranch();
                        }
                    }
                    else {
                        Toast.makeText(OwnerAddBranchActivity.this, "You can't be a manager and work at the same time, please create a new account if you want to also work in this branch as yourself", Toast.LENGTH_LONG).show();
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                }
                else {
                    username.setError("Username not found, double check what you entered");
                    if(dialog != null)
                    {
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
                if(dialog != null)
                {
                    dialog.dismiss();
                }
            }
        });
    }

    /** Check for user but don't run addBranch() */
    private void checkUsername(String tUsername)
    {
        checkUsername(tUsername,false);
    }

    /** Start the process to either take image or pick image, from dialog (change that to make it more pro) */
    private void startImageTakingOrPicking() {
        final CharSequence[] items = { "Take Photo from Camera", "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo from Camera")) {
                    Intent intent = new Intent(OwnerAddBranchActivity.this, CameraActivity.class);
                    startActivityForResult(intent,Starter.Constants.ADD_BRANCH_REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            Starter.Constants.ADD_BRANCH_REQUEST_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    /** Start location picker from google's api */
    private void startLocationPicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), Starter.Constants.PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the result from Image picker, image from camera, or place picker
     */
    //TODO: Fix crop loading issue
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == Crop.REQUEST_CROP){
            imageUri = Crop.getOutput(data);
            branchImage.setVisibility(View.VISIBLE);
            Glide.with(this).load(imageUri).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(branchImage);
            pickImage.setVisibility(View.GONE);
        }
        if(resultCode != RESULT_OK);
        if (resultCode == RESULT_OK && requestCode == Starter.Constants.PLACE_PICKER_REQUEST) {
            processPlacePicker(data);
        }
        else if (resultCode == RESULT_OK && requestCode == Starter.Constants.ADD_BRANCH_REQUEST_CAMERA)
        {
            imageUri = data.getData();
            branchImage.setVisibility(View.VISIBLE);
            Glide.with(this).load(imageUri).into(branchImage);
            pickImage.setVisibility(View.GONE);
        }
        else if (resultCode == RESULT_OK && requestCode == Starter.Constants.ADD_BRANCH_REQUEST_FILE) {
            Uri selectedImageUri = data.getData();
            destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
            Crop.of(selectedImageUri, destination).asSquare().start(this);
        }
    }

    private Bitmap rotateIfNeccessary(Uri imageUri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            ExifInterface exif = new ExifInterface(imageUri.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0f) {matrix.preRotate(rotationInDegrees);}
            Bitmap bitmap1 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            return bitmap1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    /** Take the data create GeoPoint and save it to branch, display map as lite mode and zoomed in */
    private void processPlacePicker(Intent data) {
        addPoint.setVisibility(View.GONE);
        mapParentLayout.setVisibility(View.VISIBLE);



        Place place = PlacePicker.getPlace(this, data);
        LatLng llPlace = place.getLatLng();

        GoogleMapOptions options = new GoogleMapOptions()
                .liteMode(true)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false)
                .mapToolbarEnabled(false)
                .camera(new CameraPosition(llPlace,17f,0,0));
        mapFragment = MapFragment.newInstance(options);
        mapFragment.getMapAsync(this);

        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.owner_add_branch_activity_maplayout, mapFragment);
        fragmentTransaction.commit();


        GeoPoint point = new GeoPoint(llPlace.latitude,llPlace.longitude);
        branch.setLocation(point);

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(llPlace.latitude, llPlace.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address.setText(addresses.get(0).getAddressLine(0));
            branch.setAddress(addresses.get(0).getAddressLine(0));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mapParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationPicker();
            }
        });
    }

    /** Set location according to branch, set click listener to start location picker again */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions().position(new LatLng(branch.getLocation().getLatitude(),branch.getLocation().getLongitude())));
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                startLocationPicker();
            }
        });
    }
}
