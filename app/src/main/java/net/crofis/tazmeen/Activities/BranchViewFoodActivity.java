package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;

import net.crofis.tazmeen.Adapters.BranchViewFoodAdapter;
import net.crofis.tazmeen.Adapters.BuyerPreviewOrderAdapter;
import net.crofis.tazmeen.Classes.Branch;
import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.FoodExtra;
import net.crofis.tazmeen.Classes.FoodPick;
import net.crofis.tazmeen.Classes.Order;
import net.crofis.tazmeen.Classes.OrderDetails;
import net.crofis.tazmeen.Classes.OrderDetailsFoodExtra;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;
import net.crofis.tazmeen.Services.Starter;
import net.crofis.ui.dialog.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class BranchViewFoodActivity extends AppCompatActivity {

    private static final String TAG = "BranchViewFoodActivity";

    /** To load the image on scrollable toolbar */
    ImageView image;

    /** FoodList to load the food items */
    RecyclerView foodList;

    /** BranchViewFoodAdapter that will hold the food list and FoodExtra list */
    BranchViewFoodAdapter adapter;

    /** FloatingActionButton to get the user order */
    FloatingActionButton order;

    /** User Picked Food with all the extras */
    ArrayList<FoodPick> pickedFoods;

    /** Activity toolbar */
    Toolbar toolbar;

    /** Current branch */
    Branch branch;

    /** Current logged in user if exists */
    BackendlessUser user;

    TextView noFoodWarn;

    LoadingDialog dialog;

    Order orderF;

    /** This activity displays the menu of the restaurant */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_view_food);

        initUI();
    }

    private void initUI() {
        /** Find the views */

        image = (ImageView)findViewById(R.id.branch_view_food_activity_image);

        foodList = (RecyclerView)findViewById(R.id.branch_view_food_activity_foodlist);

        foodList.setLayoutManager(new LinearLayoutManager(this));

        order = (FloatingActionButton)findViewById(R.id.branch_view_food_activity_order);

        pickedFoods = new ArrayList<>();

        noFoodWarn = (TextView)findViewById(R.id.branch_view_food_activity_nofoodwarn);

        /** Listeners */
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap b = null;
                String branchId = null;
                if (Integer.parseInt(user.getProperty("userRole").toString()) == Starter.Constants.USER_ROLE_WORKER) {
                    b = (HashMap) user.getProperty("workBranch");
                    branchId = b.get("objectId").toString();
                }
                else branchId = "HE IS NOT A WORKER";
                if (branch.getObjectId().equals(branchId))
                {
                    Toast.makeText(BranchViewFoodActivity.this, "You can't order! you work here!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    pickedFoods = adapter.combineAllPicked();
                    orderF = null;
                    orderF = new Order();

                    /**
                     * Add the normal stuff here
                     */

                    GeoPoint location = new GeoPoint();
                    location.setLatitude(Starter.Variables.location.getLatitude());
                    location.setLongitude(Starter.Variables.location.getLongitude());

                    location.addCategory("orders");

                    orderF.setLocation(location);

                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(BranchViewFoodActivity.this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    orderF.setAddress(addresses.get(0).getAddressLine(0));

                    orderF.setStatus(Starter.Constants.ORDER_STATUS_PENDING);

                    ArrayList<OrderDetails> details = new ArrayList<OrderDetails>();

                    for (FoodPick pick :
                            pickedFoods) {
                        OrderDetails detail = new OrderDetails();
                        detail.setFood(pick.getFood());
                        ArrayList<OrderDetailsFoodExtra> temp = new ArrayList<OrderDetailsFoodExtra>();
                        int count = 0;
                        for (ArrayList<FoodExtra> extraList :
                                pick.getAllExtras()) {
                            OrderDetailsFoodExtra temp1 = new OrderDetailsFoodExtra();
                            temp1.setFoodExtras(extraList);
                            temp.add(temp1);
                        }
                        detail.setFoodExtraDetails(temp);
                        details.add(detail);
                    }

                    orderF.setDetails(details);

                    displayDialog(orderF);

                    /**
                    branch.getOrders().add(order);


                    branch.saveAsync(new AsyncCallback<Branch>() {
                        @Override
                        public void handleResponse(Branch response) {
                            String whereClause = "workBranch.objectId = '" + response.getObjectId() + "'";
                            BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
                            Backendless.Persistence.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
                                @Override
                                public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                                    if (response.getData().size() == 1) {
                                        BackendlessUser user = response.getData().get(0);
                                        Starter.sendPush(response.getData().get(0).getProperty("currentDevice").toString(),"You have a new order","You have a new order","You have a new order to process, go to your branch management page to view it","new Order");
                                        finish();
                                    }
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    Log.e(TAG, "handleFault: " + fault.getMessage());
                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                        }
                    });
                    */
                }
            }
        });


        /** Load from Bundle intent */
        Bundle extras = getIntent().getExtras();
        toolbar = (Toolbar) findViewById(R.id.branch_view_food_activity_toolbar);
        //This is done in order for changing the title work
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        /** Get the json string that was inside the marker title */

        JSONObject object = null;
        try {
            /** get the string */
            object = new JSONObject(extras.getString("data"));

            /** Set the title */
            if (toolbar != null) {
                toolbar.setTitle(object.getString("name"));
            }

            /** Load the image on the scrollable toolbar */
            Glide.with(this).load(object.get("imageUrl")).asBitmap().into(image);

            /** Gets the food list for the restaurant */
            getFoodList(object.getString("branchId"));

            /** Get branch */
            getBranch(object.getString("branchId"));

            /** Check if branch is open */
            if(object.getString("isOpen").equals("false"))
            {
                order.setEnabled(false);
                Toast.makeText(BranchViewFoodActivity.this, "This branch is not openned, please choose another branch if you plan to order.", Toast.LENGTH_LONG).show();
            }

            /** Check if user is logged in */
            if(!Backendless.UserService.isValidLogin() && Backendless.UserService.CurrentUser() == null)
            {
                order.setEnabled(false);
                Toast.makeText(BranchViewFoodActivity.this, "Please create an account to order", Toast.LENGTH_LONG).show();
            }
            else
            {
                user = Backendless.UserService.CurrentUser();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /** Get Branch from BackendLess */
    private void getBranch(String objectId){
        Backendless.Persistence.of(Branch.class).findById(objectId, new AsyncCallback<Branch>() {
            @Override
            public void handleResponse(Branch response) {
                branch = response;
                order.show(true);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }


    /** Gets the food menu from the restaurant by a branch objectId */
    private void getFoodList(String objectId) {
        String whereClause = "branches.objectId = '" + objectId + "'";
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if (response.getData().size() == 1)
                {
                    if (response.getData().size() > 0) {
                        adapter = new BranchViewFoodAdapter(BranchViewFoodActivity.this, (ArrayList<Food>) response.getData().get(0).getFoods());
                        foodList.setAdapter(adapter);

                        //TODO: add if there are no user promote him to login or register
                        foodList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if(dy > 20 && order.getVisibility() == View.VISIBLE)
                                {
                                    order.hide(true);
                                }
                                else if(dy < -20 && order.getVisibility() == View.INVISIBLE)
                                {
                                    order.show(true);
                                }
                            }
                        });
                    } else {
                        noFoodWarn.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    private void displayDialog(final Order order)
    {
        if (order.getDetails().size() != 0) {
            View v = LayoutInflater.from(this).inflate(R.layout.dialog_buyer_orderpreview,null);
            RecyclerView orderList = (RecyclerView)v.findViewById(R.id.buyer_orderpreview_dialog_previewlist);
            Button accept = (Button)v.findViewById(R.id.buyer_orderpreview_dialog_accept);
            Button deny = (Button)v.findViewById(R.id.buyer_orderpreview_dialog_cancel);

            final AlertDialog dialog = new AlertDialog.Builder(this).setView(v).setTitle("Preview your order").create();
            dialog.setCancelable(true);
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final LoadingDialog dialog1 = new LoadingDialog(BranchViewFoodActivity.this,"Placing order...");

                    branch.getOrders().add(order);

                    dialog1.show();

                    dialog1.getMessage().setText("Saving your order...");
                    branch.saveAsync(new AsyncCallback<Branch>() {
                        @Override
                        public void handleResponse(Branch response) {
                            dialog1.getMessage().setText("Updating info...");
                            String whereClause = "workBranch.objectId = '" + response.getObjectId() + "'";
                            BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
                            Backendless.Persistence.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
                                @Override
                                public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                                    if (response.getData().size() == 1) {
                                        dialog1.dismiss();
                                        BackendlessUser user = response.getData().get(0);
                                        Starter.sendPush(response.getData().get(0).getProperty("currentDevice").toString(),"You have a new order","You have a new order","You have a new order to process, go to your branch management page to view it",Starter.Constants.NOTIFICATION_MESSAGE_NEW_ORDER);
                                        finish();
                                        startActivity(new Intent(BranchViewFoodActivity.this,PreviousPurchase.class));
                                    }
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    dialog.dismiss();
                                    Log.e(TAG, "handleFault: " + fault.getMessage());
                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            dialog.dismiss();
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                        }
                    });
                }
            });

            deny.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            BuyerPreviewOrderAdapter adapter = new BuyerPreviewOrderAdapter(order,this);
            orderList.setLayoutManager(new LinearLayoutManager(this));
            orderList.setAdapter(adapter);
            dialog.show();
        }
        else
        {
            Toast.makeText(BranchViewFoodActivity.this, "Please choose something", Toast.LENGTH_SHORT).show();
        }
    }
}
