package net.crofis.tazmeen.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.github.clans.fab.FloatingActionButton;

import net.crofis.tazmeen.Adapters.OwnerViewFoodAdapter;
import net.crofis.tazmeen.Classes.Food;
import net.crofis.tazmeen.Classes.Restaurant;
import net.crofis.tazmeen.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OwnerViewFoodsActivity extends AppCompatActivity {

    private static final String TAG = "OwnerViewFoodsActivity";

    FloatingActionButton addFood;

    RecyclerView foodRecyclerView;

    OwnerViewFoodAdapter adapter;

    TextView noFoodWarning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_view_foods);

        initUI();
    }

    private void initUI() {

        getSupportActionBar().setTitle("Foods");

        addFood = (FloatingActionButton)findViewById(R.id.owner_view_food_activity_addfood);

        foodRecyclerView = (RecyclerView)findViewById(R.id.owner_view_food_activity_foodlist);

        foodRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        noFoodWarning = (TextView)findViewById(R.id.owner_view_food_activity_nofoodwarning);

        addFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(OwnerViewFoodsActivity.this,OwnerAddFoodActivity.class),1111);
            }
        });

        fetchFood();
    }

    private void fetchFood() {
        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
        BackendlessDataQuery query = new BackendlessDataQuery(whereClause);
        Backendless.Persistence.of(Restaurant.class).find(query, new AsyncCallback<BackendlessCollection<Restaurant>>() {
            @Override
            public void handleResponse(BackendlessCollection<Restaurant> response) {
                if (response.getData().size() == 1)
                {
                    ArrayList<Food> foods = (ArrayList<Food>) response.getData().get(0).getFoods();
                    if (foods.size() > 0) {
//                        Collections.reverse(foods);
                        Collections.sort(foods, new Comparator<Food>() {
                            @Override
                            public int compare(Food lhs, Food rhs) {
                                return lhs.getCreated().compareTo(rhs.getCreated());
                            }
                        });
                        Collections.reverse(foods);
                        adapter = new OwnerViewFoodAdapter(OwnerViewFoodsActivity.this, foods);
                        foodRecyclerView.setAdapter(adapter);
                        foodRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if(dy > 20 && addFood.getVisibility() == View.VISIBLE)
                                {
                                    addFood.hide(true);
                                }
                                else if(dy < -20 && addFood.getVisibility() == View.INVISIBLE)
                                {
                                    addFood.show(true);
                                }
                            }
                        });
                    }
                    else{
                        noFoodWarning.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK)
        {
            fetchFood();
            noFoodWarning.setVisibility(View.INVISIBLE);
        }
    }
}
