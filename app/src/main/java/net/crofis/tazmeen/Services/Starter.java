package net.crofis.tazmeen.Services;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.Messaging;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.DeliveryOptions;
import com.backendless.messaging.MessageStatus;
import com.backendless.messaging.PublishOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import net.crofis.tazmeen.R;

/**
 * Created by orsan on 08-Apr-16.
 */
public class Starter extends Application implements Application.ActivityLifecycleCallbacks {

    /** Tag constant used for logging */
    private static final String TAG = "Starter";

    @Override
    public void onCreate() {
        super.onCreate();

        /** Logs app start */
        Log.i(TAG, "onCreate: Application started");

        /** Init BackEndless */
        initBackendless();
        Log.i(TAG, "onCreate: Backendless initialized");

        /** Start push notification service and register device */
        registerDevice();
        Log.i(TAG, "onCreate: Device registered to GCM and BackendLess");

        /** Start LocationService*/
        startLocationService();
        Log.i(TAG, "onCreate: LocationService Started");

        /** Check for user session */
        //Changed from starter to main activity so it changes the UI changes work correctly
        Log.i(TAG, "onCreate: Going to MainActivity");

        /** Register ActivityLifeCyclerCallbacks */
        registerActivityLifecycleCallbacks(this);
    }

    /** Init BackEndless service */
    private void initBackendless() {
        //Note, saving the strings inside resource did not work for some reason
        Backendless.initApp(this,"C00BCC97-F0C2-8E7A-FF21-0C5F28636900","CFCA44BB-B298-4721-FF0B-DCBD989DDC00","v1");
    }

    /** Register deviceId to BackendLess*/
    private void registerDevice() {
        Backendless.Messaging.registerDevice("415316482440", new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Log.i(TAG, "handleResponse: Device has been registered");
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: Error registering device" + fault.getMessage());
            }
        });
    }

    /** Start location service */
    private void startLocationService() {
        startService(new Intent(getApplicationContext(), LocationService.class));
    }

    /** Check for user session and retrieve user, update currentDevice property, set as CurrentUser */
    //TODO: Create thread because main activity starts before it gets a chance for the user session to load
    private void checkForUser() {

    }

    /**
     * Static class to hold off the important data to transfer around the app
     */
    public static class Variables{
        public static Location location;

        public static Integer user_role;

        public static LatLngBounds bounds;

        public static Boolean applicationOnPause;

        public static Boolean applicationStopped;

        public static Activity currentActivity;

        public static String currentActivityName;
    }

    /**
     * Static class to save constants keywords for better code
     */
    public static class Constants{

        public static final String LOCATION_SERVICE_RESULT = "com.crofis.tazmeen.Services.LocationService.LOCATION_RESULT";

        public static final String LOCATION_SERVICE_MESSAGE = "com.crofis.tazmeen.Services.LocationService.LOCATION_MESSAGE";

        public static final String ORDER_STATUS_PENDING = "pending";
        public static final String ORDER_STATUS_MAKING = "making";
        public static final String ORDER_STATUS_CANCELED = "canceled";

        public static final Integer USER_ROLE_NORMAL = 0;

        public static final Integer USER_ROLE_WORKER = 1;

        public static final Integer USER_ROLE_OWNER = 2;

        public static final Integer LOGIN_ACTIVITY_RESULT = 3;

        public static final Integer PLACE_PICKER_REQUEST = 4;

        public static final Integer ADD_BRANCH_REQUEST_CAMERA = 5;

        public static final Integer ADD_BRANCH_REQUEST_FILE = 6;

        public static int ADD_EXTRA_REQUEST = 233;

        public static int ADD_FOOD_REQUEST_CAMERA = 310;

        public static int ADD_FOOD_REQUEST_FILE = 4411;

        public static final Integer REGISTER_PASSWORD_LENGTH_MIN = 8;

        public static final Integer REGISTER_PASSWORD_LENGTH_MAX = 32;

        public static final Integer REGISTER_USERNAME_LENGTH_MIN = 6;

        public static final Integer REGISTER_USERNAME_LENGTH_MAX = 20;

        public static final String NOTIFICATION_MESSAGE_NEW_ORDER = "neworder";

        public static final String NOTIFICATION_MESSAGE_ACCEPTED = "accepted";

        public static final String NOTIFICATION_MESSAGE_DENIED = "denied";

        public static final String NOTIFICATION_MESSAGE_DONE = "done";

        public static final Integer NOTIFICATION_MESSAGE_REQUEST_CODE = 4561;
    }

    public static void sendPush (String deviceId, String ticker, String title, String text, String message)
    {
        if (deviceId != null && !deviceId.equals("")) {
            DeliveryOptions deliveryOptions = new DeliveryOptions();
            deliveryOptions.addPushSinglecast( deviceId );

            PublishOptions publishOptions = new PublishOptions();
            publishOptions.putHeader( "android-ticker-text", ticker );
            publishOptions.putHeader( "android-content-title", title );
            publishOptions.putHeader( "android-content-text", text );

            Backendless.Messaging.publish(message, publishOptions, deliveryOptions, new AsyncCallback<MessageStatus>() {
                @Override
                public void handleResponse(MessageStatus response) {
                    Log.d(TAG, "handleResponse: " + response.getStatus().toString());
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "handleFault: " + fault);
                }
            });
        }
    }


    /** Activity life cycle callbacks */
    @Override
    public void onActivityCreated(Activity arg0, Bundle arg1) {
        Log.i("Starter:","onActivityCreated");

    }
    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i("Starter:","onActivityDestroyed ");

    }
    @Override
    public void onActivityPaused(Activity activity) {
        Variables.applicationOnPause = true;
        Log.i("Starter:","onActivityPaused "+activity.getClass());

    }
    @Override
    public void onActivityResumed(Activity activity) {
        Variables.applicationOnPause = false;
        Variables.applicationStopped = false;
        Variables.currentActivity = activity;
        Variables.currentActivityName = activity.getClass().getSimpleName();
        Log.i("Starter:","onActivityResumed "+activity.getClass());
        Log.i("Starter:","onActivityPaused "+activity.getClass().getSimpleName());
    }
    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i("Starter:","onActivitySaveInstanceState");

    }
    @Override
    public void onActivityStarted(Activity activity) {
        Log.i("Starter:","onActivityStarted");

    }
    @Override
    public void onActivityStopped(Activity activity) {
        Variables.applicationStopped = true;
        Log.i("Starter:","onActivityStopped");

    }
    /** Activity life cycle callbacks END*/
}
