package net.crofis.tazmeen.Services;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.messaging.PublishOptions;
import com.backendless.push.BackendlessBroadcastReceiver;

import net.crofis.tazmeen.Activities.BranchManagerActivity;
import net.crofis.tazmeen.Activities.MainActivity;
import net.crofis.tazmeen.Activities.PreviousPurchase;
import net.crofis.tazmeen.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Windows on 16-May-16.
 */
public class TazmeenReciever extends BackendlessBroadcastReceiver {

    /**
     * Notification types
     * Seller stats:
     *  new order recieved
     * Buyer stats:
     *  order rejected
     *  order accepted
     *  order denied
     *
     * Messages to check:
     * accepted -> everyone
     * denied -> everyone
     * done -> everyone
     * neworder -> buyer only
     *
     *  Actions:
     *  (everyone)if app is paused on click notification start activity accordingly
     *  (buyer)if app is not paused and in previous purchase activity, refresh (or reinvoke getOrders, with sound and vibrate)
     *  (seller)if app is not paused and in branch manager activity, refresh (or invoke getOrders, with sound and vibrate)
     *  note: before starting activity check if there is a user and if its role
     */


    private static final String TAG = "TazmeenReciever";

    @Override
    public boolean onMessage(Context context, Intent intent) {
        if (!Starter.Variables.applicationOnPause&&(Starter.Variables.currentActivityName.equals("BranchManagerActivity")||Starter.Variables.currentActivityName.equals("PreviousPurchase")))
        {
            if(Starter.Variables.currentActivityName.equals("BranchManagerActivity"))
            {
                ((BranchManagerActivity)Starter.Variables.currentActivity).getOrders();
                Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(300);
                MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.bell);
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.start();
            }
            else if (Starter.Variables.currentActivityName.equals("PreviousPurchase"))
            {
                ((PreviousPurchase)Starter.Variables.currentActivity).getOrders();
                Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(300);
                MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.bell);
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.start();
            }
        }
        else
        {
            CharSequence tickerText = intent.getStringExtra( PublishOptions.ANDROID_TICKER_TEXT_TAG );
            CharSequence contentTitle = intent.getStringExtra( PublishOptions.ANDROID_CONTENT_TITLE_TAG );
            CharSequence contentText = intent.getStringExtra( PublishOptions.ANDROID_CONTENT_TEXT_TAG );
            String message = intent.getStringExtra("message");


            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            //TODO: Fill icon
            builder.setSmallIcon(R.drawable.ic_restaurant).setContentTitle(contentTitle).setContentText(contentText).setTicker(tickerText);

            Intent backIntent = new Intent(context, MainActivity.class);
            backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


            Intent newIntent = null;
            switch (message)
            {
                case Starter.Constants.NOTIFICATION_MESSAGE_NEW_ORDER:
                    newIntent = new Intent(context, BranchManagerActivity.class);
                    break;
                case Starter.Constants.NOTIFICATION_MESSAGE_ACCEPTED:
                case Starter.Constants.NOTIFICATION_MESSAGE_DENIED:
                case Starter.Constants.NOTIFICATION_MESSAGE_DONE:
                    newIntent = new Intent(context, PreviousPurchase.class);
                    newIntent.putExtra("whatever","yo");
                    break;
            }

            if(newIntent != null)
            {
                if (Starter.Variables.applicationOnPause) {
                    if (!Starter.Variables.applicationStopped) {
                        final PendingIntent pendingIntent = PendingIntent.getActivities(context, Starter.Constants.NOTIFICATION_MESSAGE_REQUEST_CODE,
                                new Intent[] {backIntent, newIntent}, PendingIntent.FLAG_ONE_SHOT);
                        builder.setContentIntent(pendingIntent);
                    } else {
                        final PendingIntent pendingIntent = PendingIntent.getActivity(context, Starter.Constants.NOTIFICATION_MESSAGE_REQUEST_CODE,
                                newIntent, PendingIntent.FLAG_ONE_SHOT);
                        builder.setContentIntent(pendingIntent);
                    }
                } else {
                    final PendingIntent pendingIntent = PendingIntent.getActivity(context, Starter.Constants.NOTIFICATION_MESSAGE_REQUEST_CODE,
                            newIntent, PendingIntent.FLAG_ONE_SHOT);
                    builder.setContentIntent(pendingIntent);
                }
            }

            Notification notification = builder.build();
            notification.sound = Uri.parse("android.resource://net.crofis.tazmeen/" + R.raw.bell);
            notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            NotificationManagerCompat.from(context).notify(4867,notification);
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(300);
            MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.bell);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.start();
        }
        return false;
    }
}
